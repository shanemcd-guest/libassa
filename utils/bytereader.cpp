// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: bytereader.cpp,v 1.8 2006/07/23 02:26:45 vlg Exp $
//------------------------------------------------------------------------------
//
// Author: Vladislav Grinchenko
// Date: May 29 2001
//
//------------------------------------------------------------------------------
static const char help_msg[]=
"----------------------------------------------------------------------------\n"
" NAME:                                                                      \n"
"                                                                            \n"
"   bytereader                                                               \n"
"                                                                            \n"
" DESCRIPTION:                                                               \n"
"                                                                            \n"
"  Read bytes from the connected application and display them to stdout.     \n"
"  Option {-p, --port NAME} defines listening port name to be used.          \n"
"                                                                            \n"
" USAGE:                                                                     \n"
"                                                                            \n"
"   shell>  bytereader [OPTIONS]                                             \n"
"                                                                            \n"
" OPTIONS:                                                                   \n"
"                                                                            \n"
" -b, --daemon BOOL      - run process as true UNIX daemon                   \n"
" -d, --log-stdout BOOL  - write debug to standard output                    \n"
" -D, --log-file NAME    - write debug to NAME file                          \n"
" -h, --help             - print this message                                \n"
" -m, --mask MASK        - mask (default: DBG_ALL = 0x7fffffff)              \n"
" -p, --port NAME        - tcp/ip port NAME (default - procname)             \n"
" -v, --version          - print version number                              \n"
" -z, --log-size NUM     - maximum size debug file can reach (default: 10Mb) \n"
"----------------------------------------------------------------------------";

#include <iostream>
using namespace std;

#include "assa/Assure.h"
#include "assa/GenServer.h"
#include "assa/Singleton.h"
#include "assa/INETAddress.h"
#include "assa/IPv4Socket.h"
#include "assa/Reactor.h"
#include "assa/ServiceHandler.h"
#include "assa/Acceptor.h"

using namespace ASSA;

/*-- BTREADER ----------------------------------------------------------------*/

class BTReader : public ServiceHandler<IPv4Socket>
{
public:
	BTReader (IPv4Socket*);
	~BTReader ();

	virtual int open (void);
	virtual int handle_read (int);
	virtual int handle_close (int);
private:
	static unsigned int m_count;
};

unsigned int BTReader::m_count = 0;

/*-- BYTEREADER --------------------------------------------------------------*/

class ByteReader : 
	public GenServer,
	public Singleton<ByteReader>
{
public:
	ByteReader ();
	~ByteReader ();

	virtual void init_service ();
	virtual void process_events ();

private:
	Acceptor<BTReader, IPv4Socket>* m_acceptor;
};

ASSA_DECL_SINGLETON(ByteReader);

/*-- BTREADER'S MEMBER FUNCTIONS ---------------------------------------------*/

BTReader::BTReader (IPv4Socket* my_socket_) :
	ServiceHandler<IPv4Socket>(my_socket_)
{
	trace("BTReader::BTReader"); 
}

BTReader::~BTReader ()
{
	trace("BTReader::~BTReader"); 
}

int
BTReader::open (void)
{
	trace("BTReader::open"); 

	if (BTReader::m_count != 0) {
		return -1;
	}
	IPv4Socket& s = *this;

	ByteReader::get_instance()->get_reactor()->
		registerIOHandler (this, s.getHandler(), READ_EVENT);

	BTReader::m_count++;

	return 0;
}

int
BTReader::handle_read (int fd_)
{
	trace("BTReader::handle_read"); 
	
	IPv4Socket& s = *this;
	if (s.getHandler () != fd_) return -1;

	char buf[81];
	register int len;

	while ((len = s.read (buf, 80)) > 0) 
	{
		buf[len] = '\0';
		cout << buf;
	}
	cout << flush;
	return BYTES_LEFT_IN_SOCKBUF(s);
}

int
BTReader::handle_close (int fd_)
{
	trace("BTReader::handle_close"); 

	BTReader::m_count--;
	delete this;
	return 0;
}

/*-- BYTESUCKER'S MEMBER FUNCTIONS -------------------------------------------*/

ByteReader::ByteReader ()
{
	trace("ByteReader::ByteReader"); 

	rm_opt ('f', "config-file");
	rm_opt ('n', "instance");
	rm_opt ('s', "set-name");
	rm_opt ('t', "comm-timeout");
	
	m_mask = 0;
}

ByteReader::~ByteReader ()
{
	trace("ByteReader::~ByteReader"); 
	
	if (m_acceptor != NULL)	{
		m_acceptor->close ();
		delete m_acceptor;
		m_acceptor = NULL;
	}
}

void
ByteReader::init_service ()
{
	trace("ByteReader::init_service"); 
	
	m_acceptor = new Acceptor<BTReader, IPv4Socket> (get_reactor ());
	INETAddress listening_address (get_port ().c_str ());
	Assure_exit (!listening_address.bad ());
	Assure_exit (m_acceptor->open (listening_address) == 0);
}

void
ByteReader::process_events ()
{
	trace("ByteReader::process_events"); 
	
	while (service_is_active ()) { 
		get_reactor ()->waitForEvents ();
	}
}


int main (int argc, char* argv[])
{
	trace(argv[0]); 
	
	static const char release[] = "0.0.1";
	int patch_level = 0;

	ByteReader& server = *ByteReader::get_instance (); 

	server.set_version (release, patch_level); 
	server.set_author ("Vladislav Grinchenko"); 
	server.init (&argc, argv, help_msg); 

	server.init_service   ();
	server.process_events ();

	return 0;
}

