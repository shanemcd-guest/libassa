// -*- c++ -*-
// Generated by assa-genesis
//------------------------------------------------------------------------------
// $Id: assa-hexdump.cpp,v 1.1 2006/07/23 02:47:57 vlg Exp $
//------------------------------------------------------------------------------
//                            assa-hexdump.cpp
//------------------------------------------------------------------------------
//  Copyright (c) 2006 by Vladislav Grinchenko 
//
//  This program is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU General Public License   
//  as published by the Free Software Foundation; either version  
//  2 of the License, or (at your option) any later version.      
//------------------------------------------------------------------------------
//
// Date   : Tue Jul 18 11:48:09 2006
//
//------------------------------------------------------------------------------

static const char help_msg[]=
"                                                                            \n"
" NAME:                                                                      \n"
"                                                                            \n"
"   assa-hexdump                                                             \n"
"                                                                            \n"
" DESCRIPTION:                                                               \n"
"                                                                            \n"
"   Dump the contents of the file in hex to standard output.                 \n"
"                                                                            \n"
" USAGE:                                                                     \n"
"                                                                            \n"
"   shell>  assa-hexdump [OPTIONS] INPUT-FILE                                \n"
"                                                                            \n"
" OPTIONS:                                                                   \n"
"                                                                            \n"
" -h, --help              - Print this messag                                \n"
" -v, --version           - Print version number                             \n"
"                                                                            \n"
"                                                                           \n";
//------------------------------------------------------------------------------

#ifdef HAVE_CONFIG_H
#    include "config.h"
#endif

#include <fstream>
#include <string>
using std::string;

#include <assa/Assure.h>
#include <assa/GenServer.h>
#include <assa/Singleton.h>
#include <assa/TimeVal.h>
#include <assa/MemDump.h>

class HexDump :
    public ASSA::GenServer,
    public ASSA::Singleton<HexDump>
{
public:
    HexDump ();

    virtual void init_service ();
    virtual void process_events ();

private:
    // An example of processing positional arguments
    virtual void pos_arg (const char* arg_);

private:
    string  m_in_fname;
	int     m_pos_arg_count;
};


/* Useful definitions */

#define HEXDUMP  HexDump::get_instance()
#define REACTOR HEXDUMP->get_reactor()


// Static declarations mandated by Singleton class
ASSA_DECL_SINGLETON(HexDump);

HexDump::
HexDump () : m_pos_arg_count (0)
{
    // ---Debugging---
    rm_opt ('m', "mask"         );
    rm_opt ('d', "log-stdout"   );
    rm_opt ('D', "log-file"     );
    rm_opt ('z', "log-size"     );

    // ---Configuration---
    rm_opt ('f', "config-file"  );
    rm_opt ('n', "instance"     );
    rm_opt ('p', "port"         );

    // ---Process bookkeeping---
    rm_opt ('b', "daemon"       );
    rm_opt ('l', "pidfile"      );
    rm_opt ('L', "ommit-pidfile");

    m_mask = 0;
}

void
HexDump::
pos_arg (const char* arg_)
{
    trace("HexDump::pos_arg");

    switch(m_pos_arg_count) {
    case 0:
        m_in_fname = arg_;
        break;

    case 1:
    default:
        DL((ASSA::ASSAERR,"Error: unexpected argument '%s'\n", arg_));
        DL((ASSA::ASSAERR,"Try `assa-hexdump --help` for more information.\n"));
        Assure_exit (false);
    }
    m_pos_arg_count++;
}

void
HexDump::
init_service ()
{
    trace("HexDump::init_service");

	if (m_pos_arg_count != 1) {
        DL((ASSA::ASSAERR,"Missing input file.\n"));
        DL((ASSA::ASSAERR,"Try `assa-hexdump --help` for more information.\n"));
        Assure_exit (false);
	}
}

void
HexDump::
process_events ()
{
    trace("HexDump::process_events");
	
	char buf [4096];

	std::ifstream instream (m_in_fname.c_str ());
	if (!instream) {
        DL((ASSA::ASSAERR,"Failed to open input file \"%s\".\n", 
			m_in_fname.c_str ()));
		set_exit_value (1);
		return;
	}

	while (!instream.eof ()) {
		instream.read (buf, 4096);
		ASSA::MemDump md (buf, instream.gcount ());
		std::cout << md.getMemDump () << "\n";
	}
	instream.close ();
	std::cout << std::flush;

    // Shut the service down
    m_reactor.stopReactor ();
    DL((ASSA::APP,"Service stopped!\n"));
}



int
main (int argc, char* argv[])
{
    static const char release[] = "VERSION";
    int patch_level = 0;

    HEXDUMP->set_version (release, patch_level);
    HEXDUMP->set_author  ("Vladislav Grinchenko");
    HEXDUMP->set_flags   (ASSA::GenServer::RMLOG);

    HEXDUMP->init (&argc, argv, help_msg);
 
    HEXDUMP->init_service ();
    HEXDUMP->process_events ();


    return HEXDUMP->get_exit_value ();
}

