# -*- rpm-spec -*-
#===============================================================================
#
# ASSA library RPM spec file.
#
# RPM format: name-version-release.rpm
#
# $Id: libassa.spec.in,v 1.5 2012/05/25 02:40:47 vlg Exp $
#===============================================================================

Summary:    C++ network-oriented application framework
Name:       libassa
Version:    3.5.1
Release:    0
License:    LGPL
Group:      System Environment/Libraries
URL:        http://libassa.sourceforge.net/
Source:     http://dl.sf.net/sourceforge/libassa/libassa-%{version}.tar.gz
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires: doxygen

#===============================================================================
# libassa package
#===============================================================================

%description
This package is a C++ framework for writing network-based server and
client applications. It is a non-threaded implementation of Adaptive
Communication Environment design patterns such as Reactor, Connector,
and Acceptor among others.

#===============================================================================
# libassa-devel package
#===============================================================================

%package    devel
Summary:    Headers for developing programs with libassa library
Group:      Development/Libraries
Requires:   %{name} = %{version}-%{release}
Requires:   pkgconfig libtirpc-devel

%description devel
This package contains the headers that programmers will need to develop
applications which will use libassa Library.

#===============================================================================
# libassa-devel package
#===============================================================================

%package    doc
Summary:    HTML formatted API documentation for libassa library.
Group:      Development/Libraries

%description doc
This package contains Doxygen-generated class reference of libassa Library.

#===============================================================================
# preparation section
#===============================================================================

%prep
%setup -q

#===============================================================================
# build section
#===============================================================================

%build
%configure \
    --disable-rpath      \
    --disable-static     \
    --enable-shared      \
    --disable-selftests  \
    --disable-examples

# Disable rpaths, because --disable-rpath is not supported yet.
sed -i 's|\(^hardcode_libdir_flag_spec=\).*|\1""|g' libtool
sed -i 's|\(^runpath_var=\)LD_RUN_PATH|\1DIE_RPATH_DIE|g' libtool

%{__make} %{?_smp_mflags}

#===============================================================================
# install section
#===============================================================================

%install
%{__rm} -rf %{buildroot}
make DESTDIR=%{buildroot} install

rm -rf __docdir ; mkdir __docdir
mv %{buildroot}%{_datadir}/doc/%{name}-%{version}/* __docdir

### Clean up buildroot
%{__rm} -f %{buildroot}%{_libdir}/*.la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-, root, root, -)
%doc AUTHORS COPYING ChangeLog NEWS README
%{_libdir}/*.so.*


%files  devel
%defattr(-, root, root, -)
%{_bindir}/*
%{_includedir}/assa-3.5/
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so

%files doc
%defattr(-, root, root, -)
%doc __docdir/*.html __docdir/html/


%changelog
* Wed May 23 2012 Vladislav Grinchenko <vlg[AT]users.sourceforge.net> - 3.5.1-0
- added autogen.sh
- port to gcc 4.6.3
- fixed sighands_test and regexp_test
- applied Gentoo patches 

* Sat Feb 28 2009 Caolan McNamara <caolanm[AT]fedoraproject.org> - 3.5.0-5
- add stdarg.h for va_list and stdio.h for vsnprintf

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng[AT]lists.fedoraproject.org> - 3.5.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Oct 16 2006 Michael Schwendt <mschwendt[AT]users.sf.net> - 3.4.2-3
- disable rpaths
- disable static libs
- build tests and add %%check section for them
- BR doxygen and fix inclusion of HTML files
- execute /sbin/ldconfig in scriptlets directly
- don't use %%makeinstall

* Thu Oct 12 2006 Vladislav Grinchenko <vlg[AT]users.sourceforge.net> - 3.4.2-0
- Overall makeup to adhere to the RPM packaging guidelines.

* Wed Jul 19 2006 Vladislav Grinchenko <vlg[AT]users.sourceforge.net>
- disabled tests and examples in configure step

* Sun Mar 20 2005 Vladislav Grinchenko <vlg[AT]users.sourceforge.net>
- add distribution tag

* Tue Mar  1 2005 Vladislav Grinchenko <vlg[AT]users.sourceforge.net>
- fix documentation installation error

* Sun Dec 12 2004 Vladislav Grinchenko <vlg[AT]users.sourceforge.net>
- fix postrun spelling error

* Tue Aug  6 2002 Vladislav Grinchenko <vladg[AT]erols.com>
- first public release

