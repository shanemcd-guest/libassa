// -*- c++ -*-
//------------------------------------------------------------------------------
//                          timer_queue_test.cpp
//------------------------------------------------------------------------------
// $Id: timer_queue_test.cpp,v 1.4 2006/07/20 02:30:56 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <iostream>
using namespace std;
#include <assert.h>

#include "assa/Logger.h"
#include "assa/EventHandler.h"
#include "assa/Reactor.h"
#include "assa/CommonUtils.h"
#include "assa/TimerCountdown.h"

using namespace ASSA;

struct T : public EventHandler 
{ 
	int handle_timeout (TimerId /* tid */) { return 0; }
};

int main ()
{
	::unlink ("timer_queue_test.log");
	Log::open_log_file ("timer_queue_test.log");

	std::cout << "= Running timer_queue_test Test =\n";

	TimerQueue tq;
	T eh, eh1;
	TimeVal tv (TimeVal::gettimeofday ());
	TimeVal one_sec  (1.0);
	TimeVal two_secs (2.0);
	TimeVal three_secs (3.);

	TimerId tids[4];

	tids[0] = tq.insert (&eh,  tv,              0, "now");
	tids[1] = tq.insert (&eh,  tv + one_sec,    0, "in 1 sec");
	tids[2] = tq.insert (&eh,  tv + two_secs,   0, "in 2 secs");
	tids[2] = tq.insert (&eh1, tv + three_secs, 0, "in 3 secs");

	tq.dump ();

	assert (tq.isEmpty () == false);
	assert (tq.top () == tv);
	assert (tq.remove (tids[1]));

	tq.dump ();

	int ret = tq.remove (&eh);

	tq.dump ();
	
	assert ( ret == 2);

	ASSA::Utils::sleep_for_seconds (3);
	assert (tq.expire (TimeVal::gettimeofday ()) == 1);
	assert (tq.isEmpty ());

	tq.insert (&eh, tv, 0, "dummy");	// Testing destructor

	std::cout << "Test passed\n";
	return 0;
}


