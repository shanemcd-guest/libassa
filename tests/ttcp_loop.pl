#!/usr/bin/perl  -- # -*- perl -*-
#
###############################################################################
#
#                          ttcp_loop.pl
#
###############################################################################
#
# Name: ttcp_loop.pl
#
# Synopsis:
#          ttcp_loop.pl [test_name] [port] [count]
#
# Description:
#          Retart [test_name] program as ttcp server for [count] iterations.
#          ttcp program exits after each completed data transfer from 
#          the client. [count] is needed to run it in batch.
#
# Options:
#          [test_name] - either ttcp or ttcp_assa or other compliant 
#                        ttcp modification. The base should be ttcp.c
#                        for other implementations.
#
#          [port]      - listening port.
#
#          [count]     - maximum number of restart iterations.
#
# Author: Vladislav Grinchenko
# Date:   01/05/2000
#
###############################################################################

$usage = "USAGE: ttcp_loop.pl [test_name] [port] [count]\n";

die $usage unless $#ARGV+1 == 3;

$prog  = $ARGV[0];
$port  = $ARGV[1];
$count = $ARGV[2];

while ($count) {		# Suspend process waiting till it's done.
	system "$prog -r -fm -s -p $port"; 
	$count--;
}
