// -*- c++ -*-
//------------------------------------------------------------------------------
//                           connector_test.cpp
//------------------------------------------------------------------------------
// $Id: connector_test.cpp,v 1.2 2002/12/05 03:20:17 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <iostream>
#include <assert.h>

#include "assa/Logger.h"
#include "assa/Connector.h"
#include "assa/IPv4Socket.h"
#include "assa/ServiceHandler.h"

using namespace ASSA;

class DataProcessor : public ServiceHandler<IPv4Socket>
{
public:
	int open (void) { return 0; }
};

class Pocket 
{
public:
	Pocket () { 
		trace("Pocket::Pocket");
		m_connector = new Connector<DataProcessor, IPv4Socket>; 
	}
	
	~Pocket () { 
		trace("Pocket::~Pocket");
		delete m_connector; 
	}

private:
	Connector<DataProcessor, IPv4Socket> * m_connector;
};

int main ()
{
	Pocket my_pocket;
	std::cout << "Test passed\n";
	return 0;
}

