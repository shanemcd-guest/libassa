// -*- c++ -*-
//------------------------------------------------------------------------
// $Id: autoptr_test.cpp,v 1.4 2005/03/19 17:19:24 vlg Exp $
//------------------------------------------------------------------------
//                           src/test/autoptr.C
//------------------------------------------------------------------------------
//  Copyright (c) 1999,2001,2005 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------
//
// Description:
//
//	This module tests and illustrates auto_ptr usage defined in
//	assa/AutoPtr.h file
//
// Date: March 15, 1999
//------------------------------------------------------------------------

#include <iostream>
#include <assert.h>
#include "assa/AutoPtr.h"

using namespace std;
using namespace ASSA;

/** -------------------------------------------------------------------------
 *  Parent
 ** -------------------------------------------------------------------------
 */
class VParent 
{
private:
    static int m_cnt;

public:
    VParent() { 
		cout << "-- VParent::VParent --\n";
		m_cnt++; 
    }
    ~VParent() { 
		cout << "-- VParent::~VParent --\n";
		m_cnt--; 
    }

    VParent (const VParent& /* m_ */) { 
		cout << "-- VParent::VParent(const VParent&) --\n";
		m_cnt++; 
    }
    
    VParent& operator= (const VParent& /* m_ */) { 
		cout << "-- VParent::operator= --\n";
		m_cnt++; 
		return *this;
    }

    static int getCount() { return m_cnt; }
    static void resetCount() { m_cnt = 0; }
    void printMsg() { cout << "I'm a VParent class!\n"; }
};

int VParent::m_cnt = 0;

/** -------------------------------------------------------------------------
 *  Child
 ** -------------------------------------------------------------------------
 */
class VChild : public VParent {};

void leak_mem ()
{
	cout << "-- leak_mem --\n";
	VParent* a = new VParent();

	a->printMsg();
}

void no_leak_mem ()
{
	cout << "-- no_leak_mem --\n";
	AutoPtr<VParent> a (new VParent);

	a->printMsg();
}

/** -------------------------------------------------------------------------
 *  Function
 ** -------------------------------------------------------------------------
 */
AutoPtr<VParent> foo ()
{
	AutoPtr<VParent> vptr (new VParent);
	return vptr;
}

/** --------------------------------------------------------------------------
 *  Main
 ** --------------------------------------------------------------------------
 */
int main()
{
	cout << "\n*** Testing memory allocation/deallocation ***\n";

	cout << "\nCalling leak_mem()\n";
	leak_mem();
	cout << "copies on heap still allocated: " << VParent::getCount();
	assert (VParent::getCount() == 1);
	cout << " ... ok\n";

	VParent::resetCount ();

	cout << "\nCalling no_leak_mem()\n";
	no_leak_mem();
	cout << "copies on heap still allocated: " << VParent::getCount();
	assert (VParent::getCount() == 0);
	cout << " ... ok\n";

	// How about rebinding later in the game?
	{
		AutoPtr<VParent> fp;
		bool cond = true;
		
		if (cond) {
			AutoPtr<VParent> temp (new VParent);
			fp = temp;
		}
	}

	//---------------------------------------------------------------
	// A function can behave as a source of data. When an AutoPtr
	// is returned, ownership of the returned value gets transferred
	// to the calling function.

	cout << "\n*** Testing ownership transfer by function ***\n\n";

	VParent::resetCount ();
	{
		AutoPtr<VParent> vp;
		for (int i = 0; i < 3; i++) {
			vp = foo ();
		}
	}
	
	//---------------------------------------------------------------
	cout << "\n*** Testing ownership transfer ***\n\n";

	{
		AutoPtr<VParent> p1 (new VParent());
		AutoPtr<VParent> p2;

		cout << "\nUninitialized AutoPtr is a '0' pointer ...";
		assert (p2.get() == 0);
		cout << " ok\n";
		
		cout << "Ownership transfer by assignment ... ";
		p2 = p1;
		assert (p2.get() != 0);
		cout << "ok\n";

		cout << "Ownership transfer by copy constructor ... ";
		AutoPtr<VParent> p3(p2);

		assert (p3.get() != 0);
		cout << "ok\n";
	}

	cout << "\n*** Testing inheritance handling ***\n\n";
	{
		AutoPtr<VParent> pp (new VChild);
		AutoPtr<VChild> pc (new VChild);

		pp = pc;

		AutoPtr<VParent> pp2 (pc);
	}

 	cout << "ok\n";

	// --------------------------------------------------------------
	cout << "\n*** Testing AutoPtrArray handling ***\n\n";

	{
		AutoPtrArray<VParent> vp1 (new VParent[6]);
	}

 	cout << "ok\n";

	cout << "\nTest done!\n\n";
}

