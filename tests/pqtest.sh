#! /bin/ksh
###################################################################
#
# $Id: pqtest.sh,v 1.1.1.1 2002/11/02 21:55:03 vlg Exp $
#
###################################################################
#
# Name: pqtest.sh
#
# Description:
#
#   This is a test that runs 'pq' for dataset size
#   from 10 to 1000, with max element from the interval [-1000; 1000].
#
#   If 'pq' fails, the last failed *.dat file will be left over
#   to re-run the test.
#
# Written: 1999/09/17 21:15:08 vlad 
###################################################################

integer i=10
integer j=1

while ((i < 1000))
    do
	pqtest -r 1000 -n $i >log

	if (($? != 0))
	then
	    echo
	    echo "************************************************"
	    echo "*        Failed test on # $i                   *"
	    echo "************************************************"
	    echo
	    exit 1
	fi
	i=i+1
	/bin/rm -f *.dat
	sleep 2

	echo "[$i/1000] "
	if ((j == 79))
	then
	    echo
	    j=1
	fi
done
