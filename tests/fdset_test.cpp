// -*- c++ -*-
//------------------------------------------------------------------------------
//                           fdset_test.cpp
//------------------------------------------------------------------------------
// $Id: fdset_test.cpp,v 1.2 2002/12/05 03:20:17 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <iostream>
#include <assert.h>

#include "assa/FdSet.h"
#include "assa/Logger.h"
using namespace ASSA;

int main ()
{
	int fds[] = { 0, 1, 2, 5, 15 };
	int fdsz = sizeof(fds)/sizeof(int);
	int i;
	int failed = 0;
	int total = 5;

	std::cout << "= Running fdset_test =\n";

	FdSet s1;
	std::cout << "Step1: Testing setting bits ... ";

	for (i=0; i<fdsz; i++) {
		if ( !s1.setFd (fds[i]) ) {
			std::cout << "failed!\n";
			failed++;
		}
		else {
			std::cout << fds[i] << " ";
		}
	}
	std::cout << " ok\n";
	s1.dump ();

	std::cout << "Step2: Testing bits set ... ";
	
	for (i=0; i<fdsz; i++) {
		if ( !s1.isSet (fds[i]) ) {
			std::cout << "failed!\n";
			failed++;
		}
	}
	std::cout << "ok\n";
	
	std::cout << "Step3: Testing set bits count ... ";
	if ( s1.numSet() != fdsz ) {
		std::cout << "failed! expected " << fdsz 
				  << ", reported " << s1.numSet() << std::endl;
		failed++;
	}
	else {
		std::cout << "ok\n";
	}

	std::cout << "Step4: Test bits clear ... ";

	s1.clear (fds[0]);
	s1.clear (fds[1]);
	std::cout << fds[0] << " " << fds[1] << " ";

	if ( s1.isSet(fds[0]) || s1.isSet(fds[1]) ) {
		std::cout << "failed! Reason = can't reset a single bit\n";
		failed++;
	}
	else {
		std::cout << " ok\n";
	}

	s1.dump ();

	std::cout << "Step5: Test reset ... ";
	s1.reset ();
	
	if ( s1.numSet() != 0 ) {
		std::cout << "failed! Reason = can't reset the whole set\n";
		failed++;
	}
	std::cout << "ok\n";
	
	if (failed) {
		std::cout << failed << " out of " << total << " tests failed\n";
	}
	std::cout << "Test passed\n";
	s1.dump ();

	return failed;
}

