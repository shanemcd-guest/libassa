#!/usr/bin/perl -- # -*- perl -*-
#
###############################################################################
#
#                          ttcp_test.pl
#
###############################################################################
#
# Name: ttcp_test.pl
#
# Synopsis:
#
#          ttcp_test.pl [test_name] [output] [max size] [host] [port]"
#
# Description:
#
#          This script tests throughput performance of [test_name]
#          ttcp program by sending series of 5 packets of 1K increment up
#          to [max size] (1, 2, 4, 8, 16, 32, 64K, ..., [max size]) to
#          the 5001@[host] server.
#
#          Packet size (in Kbytes) and throughput (in Mbps) pairs are
#          written in [test_name].dat file for further processing.
#
# Options:
#          [test_name] - either ttcp or ttcp_crng or other compliant
#                        ttcp modification. The base should be ttcp.c
#                        for other implementations.
#
#          [output]    - output file name
# 
#          [max size]  - maximum packet size in Kbytes. 64 is a good max.
#
#          [host]      - host where server is running.
#
#          [port]      - port server listening on.
#
# Author: Vladislav Grinchenko
# Date:   01/05/2000
#
###############################################################################

$usage = "USAGE: ttcp_test.pl [test_name] [data_file] [max size Kb] [host] [port]\n";

die $usage unless $#ARGV+1 == 5;

$prog  = $ARGV[0];
$data  = $ARGV[1];
$maxKb = $ARGV[2];
$host  = $ARGV[3];
$port  = $ARGV[4];

$msize = 1024;
$limit = $maxKb * 1024;

open (DATA,">$data") || die "Can't open output file: $data";

while ($msize <= $limit) {
  for ($i = 1; $i <= 5; $i++) {
    $Mbps = 0;
    print "Iteration# ", $i, ": $prog -t -fm -s -l $msize -p $port $host";

    @out = `$prog -t -fm -s -l $msize -p $port $host 2>/dev/null`;

    @line = split(/[ \t\n]+/, $out[1]);
    $Mbps = ($Mbps + $line[8])/2;

    print " >>> Mbps: ", $Mbps, "\n";
    sleep 1
  }
  print DATA ($msize/1024, " ", $Mbps,"\n");
  print ("--------------------------------------\n");
  $msize = $msize * 2;
}

close DATA;
