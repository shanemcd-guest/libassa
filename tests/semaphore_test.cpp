// -*- c++ -*-
//------------------------------------------------------------------------------
//                         semaphore_test.cpp
//------------------------------------------------------------------------------
// $Id: semaphore_test.cpp,v 1.5 2006/07/20 02:30:56 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <iostream>
using namespace std;

#include <assert.h>

#if !defined (WIN32)
#   include <sys/ipc.h>		// ftok(2)
#endif

#include "assa/Assure.h"
#include "assa/Fork.h"
#include "assa/Semaphore.h"
#include "assa/CommonUtils.h"

using namespace ASSA;

#if !defined (WIN32)
void fork_with_lock (Semaphore& s)
{
	trace("fork_with_lock");

	DL((TRACE,"[Parent] Forking child...\n"));
	Fork f (Fork::KILL_ON_EXIT, Fork::IGNORE_STATUS);

	if (f.isChild ()) {
		char self[] = "*-*-*-*-* [Child]";
		DL((TRACE,"Raising semaphore ...\n"));

		s.signal ();
		s.dump ();
		DL((TRACE,"Sleeping for 10 secs ...\n"));

		ASSA::Utils::sleep_for_seconds (10);
		DL((TRACE,"Child finished !\n")); 
		exit (0);
	}
}
#endif 

int main (int argc, char* argv[])
{

#if !defined (WIN32)

	string log_name ("semaphore_test.log");
	::unlink (log_name.c_str ());
	Log::open_log_file (log_name.c_str ());

	trace("semaphore_test");

	std::cout << "= Running semaphore_test Test =\n";

	key_t key = ftok ("/etc/hosts", 'S');
	assert (key != (key_t) -1);

	Semaphore s;
	s.create (key, 0);
	s.dump ();

	DL((TRACE,"Semaphore created. Press Enter to continue... \n"));

	int c;
	c = cin.get ();

	fork_with_lock (s);

	s.dump ();
	DL((TRACE,"Waiting on semaphore ...\n"));
	s.wait ();

	DL((TRACE,"Got the semaphore ...\n"));
	s.dump ();

	DL((TRACE,"Removing semaphore ...\n"));
	s.remove ();
	DL((TRACE,"Parent finished !\n"));

#endif 
	std::cout << "Test passed\n";

	return 0;
}


