#!/usr/bin/perl -- # -*- perl -*-
#
###############################################################################
#
#                            ttcp_20test.pl
#
###############################################################################
#
# Name:
#
#      ttcp_20test.pl
#
# Synopsis:
#
#      ttcp_20test.pl [host]
#
# Description:
#
#      Run ttcp_test.pl with 20 times for both "C" implementation of
#      ttcp and ASSA wrappers, collecting data for further processing by 
#      gnuplot.
#
#      Start ttcp servers on server host with following:
#
#        %	ttcp_loop.pl ttcp      5001 1000 &
#        %  ttcp_loop.pl ttcp_assa 5002 1000 &
#
# Options:
#          [host]      - host where servers are running.
#
# Author: Vladislav Grinchenko
# Date:   01/05/2000
#
###############################################################################

$usage = "USAGE: ttcp_20test.pl [host]\n";

die $usage unless $#ARGV+1 == 1;

$host  = $ARGV[0];

$proc = "ttcp";
print "Running $proc tests...\n";

foreach $count (1..20)
{
  $trgt = $proc . $count . ".dat";
  system "ttcp_test.pl $proc $trgt 64 $host 5001";
}

$proc = "ttcp_assa";
print "Running $proc tests...\n";

foreach $count (1..20)
{
  $trgt = $proc . $count . ".dat";
  system "ttcp_test.pl $proc $trgt 64 $host 5002";
}


      
