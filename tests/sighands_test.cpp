// -*- c++ -*-
//------------------------------------------------------------------------------
//                          sighands_test.cpp
//------------------------------------------------------------------------------
// $Id: sighands_test.cpp,v 1.12 2012/05/21 03:20:39 vlg Exp $
//------------------------------------------------------------------------------
const char title [] =
"                                                                           \n"\
"  NAME                                                                     \n"\
"                                                                           \n"\
"     sighands_test                                                         \n"\
"                                                                           \n"\
"  DESCRIPTION                                                              \n"\
"                                                                           \n"\
"       Test program for following classes:                                 \n"\
"            o SigHandlers                                                  \n"\
"            o SigHandlersList                                              \n"\
"            o Semaphore                                                    \n"\
"                                                                           \n"\
"  Test progress is displayed to the standard output and appended to        \n"\
"  the test log file assa_tests.log.                                        \n"\
"                                                                           \n"\
"  Program runs through all test cases regardless of whether they ended     \n"\
"  with success or an error.                                                \n"\
"                                                                           \n"\
"  RETURN                                                                   \n"\
"                                                                           \n"\
"       0 on success, 1 if at least one test failed.                        \n"\
"                                                                           \n"\
"  SYNOPSIS                                                                 \n"\
"                                                                           \n"\
"       % sighands_test [OPTIONS]                                           \n"\
"                                                                           \n"\
"  OPTIONS                                                                  \n"\
"                                                                           \n"\
" -D, --log-file NAME - debug logging file name                             \n"\
" -v, --version         - print out version number                          \n"\
" -h, --help            - print out this help information                   \n"\
"                                                                           \n"\
"  Author: Vladislav Grinchenko                                             \n"\
"  Date:   Jul 12, 2000                                                     \n";
//------------------------------------------------------------------------------
//
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

#include <sstream>
#include <fstream>		// ofstream
#include <iostream>
#include <string>
#include <list>
using std::list;
using std::string;
using namespace std;

#if !defined (WIN32)

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>

#include "assa/GenServer.h"
#include "assa/Fork.h"
#include "assa/Assure.h"
#include "assa/Handlers.h"
#include "assa/Semaphore.h"
using namespace ASSA;

// Count how may signals child process sends back to its parent
static const int REPLY_COUNT = 1;

// Global status buffer
std::ostringstream status_msg;

//------------------------------------------------------------------------------
// Global "C" signal handler and its counter.
//
// This signal handler simulates presence of third-party 'C' library
// signal handler that ASSA needs to cope with. Its disposition
// must be preserved.
// The signal handler is installed at parent initialization.
// Its counter value is initialized at the beginning of each test
// At the end of the test we examine how many times it has been evoked.
//
// The goal is to validate that the signal handler installed by the
// third-party library functions as expected.
//------------------------------------------------------------------------------
//
int usr1_C_sig_count = 0;

extern "C" {
	static void usr1_C_handler (int signum_)
	{
		trace("::usr1_C_handler");

		Assure_exit (signum_ == SIGUSR1);
		++usr1_C_sig_count;

		DL((TRACE,"<== Received SIG USR1 # %d\n", usr1_C_sig_count));
	}

	static void reset_usr1_C_handler ()
	{
	    usr1_C_sig_count = 0;
	}
};

class Test;

//------------------------------------------------------------------------------

class Child : public EventHandler
{
public:
    Child (Test* test_, long mask_);

    virtual       ~Child ();

    void          run           ();
    virtual int   handle_signal (int signum_);

private:
    sig_atomic_t  m_rcv_usr1_cnt;
    sig_atomic_t  m_rcv_usr2_cnt;
    Test*         m_test;
};

//------------------------------------------------------------------------------

class Test : public GenServer, public Singleton<Test>
{
public:
    Test ();
    ~Test ();

    virtual void  init_service   ();
    virtual void  process_events ();

    Semaphore&    get_semaphore () { return m_sem; }

    int           status () { return m_status; }

private:
    void          set_semaphore ();

    void          log_status (const string& m_);
    void          log_status (std::ostringstream& m_);

    void          catch_signals ();
    void          catch_signals_2 ();

    void          run_test1 ();
    void          run_test2 ();
    void          run_test3 ();
    void          run_test4 ();
    void          run_test5 ();

private:
    pid_t          m_child_pid;
    Child*         m_child;

    SIGUSR1Handler m_usr1;	// USER1 signal handlers
    Semaphore      m_sem;	// Semaphore
    int            m_status; // Program return status (0:ok, 1:error)
};

ASSA_DECL_SINGLETON(Test);

//------------------------------------------------------------------------------
//
// Start Class Child member functions
//   ||
//------------------------------------------------------------------------------
//
Child::
Child (Test* test_, long mask_)
	: m_rcv_usr1_cnt (0), m_rcv_usr2_cnt (0), m_test (test_)
{
    /*
      The child process is waiting indefinitely for USR1/USR2 signal,
      and, upon reception of that signal, sends back 3 signals in a
      sequence with delay of one second to its parent process.

      The child process will be SIGTERMed by parent process upon
      completion of its testing procedures.
    */

    /*--- Close parent's log file and open my own. ---*/
    Log::log_close ();

    /*---
      It took me 3 days to figure this one out!
      If old log file is not around it's not a big deal. I used to call
      Assure_exit (unlink ()) here, and OS would send SIGHUP to
      the child process, loosing log message about assertion failed
      somehow, so that I didn't see it in the log file (either parent's
      or child's)!!! 07/18/2000
      ---*/

    unlink ("sighands_chld.log");

    Log::open_log_file ("sighands_chld.log", mask_);
    Log::enable_timestamp ();
    trace("Child::Child");

    DL((TRACE,"Parent's     PID: %d\n", getppid () ));
    DL((TRACE,"My (child's) PID: %d\n", getpid () ));

    SigHandlers& sh = m_test->get_sig_manager ();

    /*--- Block SIGHUP while processing SIGUSR1/SIGUSR2 ---*/
    SigSet bs;
    bs.add (SIGHUP);

    SigAction bact;
    bact.mask (bs);

    /*--- Install signal handler ---*/
    Assure_exit (sh.install (SIGUSR1, this, &bact) == 0);
    DL((TRACE,"SIGUSR1 signal handler installed!\n"));

	Assure_exit (sh.install (SIGUSR2, this, &bact) == 0);
	DL((TRACE,"SIGUSR2 signal handler installed!\n"));
}

Child::
~Child ()
{
    trace("Child::~Child");
}

/*
    Whatever signal we get from the parent, we fire it back 3 times
    at the parent process one second apart. When we are done, we signal
    parent via semaphore so that it can check is status.
*/
int
Child::
handle_signal (int signum_)
{
    trace("Child::handle_signal");

    Assure_exit (signum_ == SIGUSR1 || signum_ == SIGUSR2);

    DL((TRACE,"==> Rcvd %s (# %d) from Parent\n",
		(signum_ == SIGUSR1) ? "SIGUSR1" : "SIGUSR2",
		(signum_ == SIGUSR1) ? ++m_rcv_usr1_cnt : ++m_rcv_usr2_cnt));

    DL((TRACE," === Signal sequence started ===\n"));

    for (int i = 0; i < REPLY_COUNT; ++i)
    {
		kill (getppid (), signum_);
		sleep (1);

		DL((TRACE,"<== Sent %s (# %d) to the parent\n",
			(signum_ == SIGUSR1) ? "SIGUSR1" : "SIGUSR2", i));
    }

    DL((TRACE," === Signal sequence completed ===\n"));
    return 0;
}

void
Child::
run ()
{
    trace("Child::run");

    /*--- Raise binary semaphore ---*/
    m_test->get_semaphore ().signal ();

    /*--- Let Reactor run processing all events (signals in our case) ---*/
    DL((TRACE," *** Running child ***\n"));

    while (m_test->service_is_active ()) {
		m_test->get_reactor ()->waitForEvents ();
    }
    DL((TRACE," *** Child exited  event loop ***\n"));
}
//
//------------------------------------------------------------------------------
//  ||
// End Class Child member functions
//
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//
// Start Class Test
//   ||
//------------------------------------------------------------------------------
//
Test::
Test()
    : m_child_pid (0), m_child (NULL), m_status (0)
{
    trace("Test::Test");

    /* Set up a shared semaphore between parent and
       child. Child will inform Parent via this semaphore
       that it is ready to receive commands (USR2 signals) from it.

       With no semaphore in place, parent is forced to sleep before sending
       command signal. Otherwise the signal could have been emitted before
       child had had a chance to install its signal handler.
    */

    set_semaphore ();
}

/*
  Set up a semaphore. In a parent-child relationship, the parent
  creates new IPC structure and the resulting identifier is then
  available to the child after the 'fork(2)'.

  We use debug file name as assured file, and 's' as project identifier.
*/
void
Test::
set_semaphore ()
{
    trace("Test::set_semaphore");

    key_t key = ftok ("/etc/hosts", 'S');
    Assure_exit (key != (key_t) -1);

    /*--- Create semaphore ---*/
    Assure_exit (m_sem.create (key, 0) != -1);
    m_sem.dump ();
}

Test::
~Test()
{
    trace("Test::~Test");

    m_sem.close ();

    if (m_child != NULL) {
		delete m_child;
    }
}

void
Test::
init_service ()
{
    trace("Test::init_service");

    /* Fork a child process - it will send us signals */

    DL((TRACE," Forking child process ...\n"));

    Fork ss (Fork::KILL_ON_EXIT, Fork::IGNORE_STATUS);

    if (ss.isChild ()) {
		m_child = new Child (this, m_mask);
		m_child->run ();
		DL((TRACE," *** Child stopped ***\n"));
		exit (0);
    }

    Log::log_close ();
    Log::open_log_file ("sighands_parent.log", m_mask);
    Log::enable_timestamp ();

    DL((TRACE," Forking completed!\n"));

    m_child_pid = ss.getChildPID ();
    DL((TRACE," Child's PID: %d\n", m_child_pid));

    /*
      Install conventional "C" handler to simulate the presence
      of a 3rd party librayr.
     */
    SigAction oact;
    SigAction act (&usr1_C_handler);

    act.register_action (SIGUSR1, &oact);

    /* Write header to the log file
     */
    std::cout << "\n============== sighands_test ==================\n"
			  << "\nTesting classes: SigHandlers, SigHandlersList, "
			  << "Semaphore\n" << std::flush;
}

void
Test::
process_events ()
{
    trace("Test::process_events");

    /*--- Block waiting for child to signal on the semaphore ---*/
    m_sem.wait ();

    /*--- Run tests ---*/

    run_test1 ();
    run_test2 ();

    run_test3 ();
    run_test4 ();

    run_test5 ();

    /*--- Complete log record ---*/
    std::cout << "\n============== sighands_test completed ========\n"
              << std::flush;
}

void
Test::
log_status (const string& m_)
{
    trace("Test::log_status");
    std::cout << m_ << flush;
    DL((TRACE," %s\n", m_.c_str ()));
}

void
Test::
log_status (std::ostringstream& m_)
{
    log_status (m_.str());
    m_.str ("");
}

void
Test::
catch_signals ()
{
    trace("Test::catch_signals");

    /* Send USR1 signal to child for triggering a signal sequence from it. */
    DL((TRACE,"<== Sending SIGUSR1 to child process PID: %d.\n", m_child_pid));

    Assure_exit (kill (m_child_pid, SIGUSR1) == 0);

    /*--- Wait for all signals for the total of 5 seconds. ---*/

    DL((TRACE," Waiting for 5 seconds on child ...\n"));
    TimeVal tv (5.0);

    while (tv != TimeVal::zeroTime ()) {
		get_reactor ()->waitForEvents (&tv);
    }
}

/**
 * Communicate with the child via signals/semaphore for test5.
 */
void
Test::
catch_signals_2 ()
{
    trace("Test::catch_signals_2");

    /** Send USR1 signal to child for triggering a signal sequence from it.
     */
    DL((TRACE,"Sending SIGUSR1 to child process PID: %d ==>\n", m_child_pid));

    Assure_exit (kill (m_child_pid, SIGUSR1) == 0);

/*
    DL((TRACE," Waiting for 6 seconds on child to reply to SIGUSR1 ...\n"));
    TimeVal tv1 (6.0);

    while (tv1 != TimeVal::zeroTime ()) {
		get_reactor ()->waitForEvents (&tv1);
    }
*/
    /** Send USR2 signal to child for triggering a signal sequence from it.
     */
    DL((TRACE,"Sending SIGUSR2 to child process PID: %d ==>\n",m_child_pid));

    Assure_exit (kill (m_child_pid, SIGUSR2) == 0);

    DL((TRACE," Waiting for 6 seconds on child to reply to SIGUSR2 ...\n"));

    TimeVal tv2 (5.0);

    while (tv2 != TimeVal::zeroTime ()) {
		get_reactor ()->waitForEvents (&tv2);
    }
}

/*-------------------------------------------------------------------
  Test 1: Testing "C" handler.
 ---------------------------------------------
 + Receive 3 signals from child process and validate the count.
 -------------------------------------------------------------------*/
void
Test::
run_test1 ()
{
    trace("Test::run_test1");

    DL((TRACE,"=== Test 1 started ===\n"));

    reset_usr1_C_handler ();

    catch_signals ();

    status_msg << "=== Test 1 ";

    if (usr1_C_sig_count != REPLY_COUNT) {
		status_msg << " failed (count != " << REPLY_COUNT << ") ===\n";
		m_status = 1;
    }
    else {
		status_msg << " ok ===\n";
    }
    log_status (status_msg);
}

/*---------------------------------------------------------------------
   Test 2: Testing EventHandler & "C" handler dispatched through
           signal dispatcher.
  ---------------------------------------------
   + Install SIGUSR1 EventHandler
   + Receive signals.
   + Validate that all 3 signals have been captured by the "C" handler.
-----------------------------------------------------------------------
*/
void
Test::
run_test2 ()
{
    trace("Test::run_test2");

    bool failed = false;

    DL((TRACE,"=== Test 2 started ===\n"));

    /*
       Install USR1 signal handler. 'Test' is EventHandler and will
       catch signals itself.
    */
    reset_usr1_C_handler ();

    m_usr1.resetState ();
    m_sig_dispatcher.install (SIGUSR1, &m_usr1);

    catch_signals ();

    status_msg.str("=== Test 2  ok ===\n");

    if (m_usr1.received_count () != REPLY_COUNT) {
		status_msg.str ("=== Test 2  failed on USR1 (count != ");
        status_msg << REPLY_COUNT << ") ===\n";
		log_status (status_msg);
		failed = true;
    }

    if (usr1_C_sig_count  != REPLY_COUNT) {
		status_msg.str ("=== Test 2  failed on \"C\" handler (count != ");
        status_msg << REPLY_COUNT << ") ===\n";
		log_status (status_msg);
		failed = true;
    }

    if (!failed ) {
        log_status (status_msg);
    }
    else {
        m_status = 1;
    }
}

/*---------------------------------------------------------------
  Test 3: Validate preservation of
 ----------------------------------------
  + Remove SIG USR1 Event Handler.
  + Receive signals.
  + Validate that "C" handler still received all signals.
 ----------------------------------------------------------------
*/
void
Test::
run_test3 ()
{

    trace("Test::run_test3");

    DL((TRACE,"=== Test 3 started ===\n"));
    m_usr1.resetState ();
    reset_usr1_C_handler ();

    m_sig_dispatcher.remove (SIGUSR1, &m_usr1);

    catch_signals ();

    status_msg << "=== Test 3 ";

    if (usr1_C_sig_count != REPLY_COUNT) {
		status_msg << " failed (count != " << REPLY_COUNT << ") ===\n";
		m_status = 1;
    }
    else {
		status_msg << " ok ===\n";
    }

    log_status (status_msg);
}

/*-------------------------------------------------------------------
   Test 4: Test preservance of 3rd party signal handler
           even after signal dispatcher is deactivated.
 ---------------------------------------------
  + Clean up signal dispatcher
  + Receive signals
  + Validate that "C" handler still received all signals.
 --------------------------------------------------------------------
 */
void
Test::
run_test4 ()
{

    trace("Test::run_test4");

    DL((TRACE,"=== Test 4 started ===\n"));

    reset_usr1_C_handler ();

    catch_signals ();

    status_msg << "=== Test 4 ";

    if (usr1_C_sig_count != REPLY_COUNT) {
		status_msg << " failed (count != " << REPLY_COUNT <<") ===\n";
		m_status = 1;
    }
    else {
		status_msg << " ok ===\n";
    }

    log_status (status_msg);
}

/*-------------------------------------------------------------------
  Test 5: Test mulitpe signals with multiple Event Handlers.
 ---------------------------------------------
   + Install N=1 usr1 and usr2 event handlers.
   + Receive both usr1 and usr2 signals.
   + Validate that "C" handler still receives all signals,
     and all event handlers received all of the signals.
---------------------------------------------------------------------*/
void
Test::
run_test5 ()
{

    trace("Test::run_test5");

    int i;
    SIGUSR1Handler* h1p = 0;
    SIGUSR2Handler* h2p = 0;
    const int SN = 20;

    list<SIGUSR1Handler*> usr1_list;
    list<SIGUSR2Handler*> usr2_list;

    DL((TRACE,"=== Test 5 started ===\n"));

    /*--- Reset C handler count ---*/
    reset_usr1_C_handler ();

    /**--- Create and install USR1 signal handlers ---
     */
    for (i = 0; i < SN; i++) {
		h1p = new SIGUSR1Handler;
		Assure_exit (m_sig_dispatcher.install (SIGUSR1, h1p) == 0);
		usr1_list.push_back (h1p);
    }

    /**--- Create and install USR2 signal handlers ---
     */
    for (i = 0; i < SN; i++) {
		h2p = new SIGUSR2Handler;
		Assure_exit (m_sig_dispatcher.install (SIGUSR2, h2p) == 0);
		usr2_list.push_back (h2p);
    }

    /**--- Catch all signals ---
     */
    catch_signals_2 ();

    /*--- Count signals ---*/
    bool seen_error = false;
    bool failed     = false;

    status_msg << "=== Test 5  ok ===\n";

    /*--- Test counters ---*/
    while (!usr1_list.empty ())
    {
		h1p = usr1_list.front ();

		if (h1p->received_count () != REPLY_COUNT)
		{
			seen_error  = true;
			failed      = true;
			status_msg.str ("=== Test 5  failed: USR1 rcvd signals ");
			status_msg << "count != " << REPLY_COUNT << " ===\n";
		}

		Assure_exit (m_sig_dispatcher.remove (SIGUSR1, h1p) == 0);
		usr1_list.pop_front ();
		delete h1p;
    }

    if (seen_error) {
        log_status (status_msg);
    }

    while (!usr2_list.empty ())
    {
		h2p = usr2_list.front ();
		if (h2p->received_count () != REPLY_COUNT)
		{
			seen_error  = true;
			failed      = true;
			status_msg.str("=== Test 5  failed: USR2 rcvd signals ");
            status_msg << "count != " << REPLY_COUNT << " ===\n";
		}

		Assure_exit (m_sig_dispatcher.remove (SIGUSR2, h2p) == 0);
		usr2_list.pop_front ();
		delete h2p;
    }

    if (seen_error) {
        log_status (status_msg);
    }

    if (usr1_C_sig_count != REPLY_COUNT) {
		failed = true;
		status_msg.str ("=== Test 5  failed: \"C\" rcvd signals ");
        status_msg << "count != " << REPLY_COUNT << " ===\n";
		log_status (status_msg);
    }

    if (!failed ) {
        log_status (status_msg);
    }
    else {
        m_status = 1;
    }
}
//
//------------------------------------------------------------------------------
//   ||
// End Class Test
//------------------------------------------------------------------------------

#endif	// !WIN32

int
main (int argc, char* argv[])
{
#if !defined (WIN32)

    int rev = 0;
    Test& t = *Test::get_instance ();

    t.set_version ("1.1", rev);
    t.set_author ("Vladislav Grinchenko");

    t.set_flags (GenServer::RMLOG);
    t.init (&argc, argv, title);

    t.init_service ();
    t.process_events ();

    exit (t.status ());

#else

	exit (0);

#endif // (WIN32)
}
