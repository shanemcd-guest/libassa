// -*- c++ -*-
//------------------------------------------------------------------------------
//                               LogConn.h
//------------------------------------------------------------------------------
// $Id: LogConn.h,v 1.2 2006/07/20 02:30:55 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2003 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// Created: Wed Apr 30 19:07:30 EDT 2003
//------------------------------------------------------------------------------
#ifndef LOGCONN_H
#define LOGCONN_H

#include <assa/ServiceHandler.h>
#include <assa/IPv4Socket.h>

/*******************************************************************************
 Class 
*******************************************************************************/

class LogConn : public ASSA::ServiceHandler<ASSA::IPv4Socket>
{
public:
	LogConn ();
	~LogConn ();

	virtual int  open ();
	virtual void close ();

	virtual int handle_read (int fd_);
	virtual int handle_close (int fd_);

private:
	int process_user_request (ASSA::IPv4Socket& s_);

private:
	char m_eor [2];	// end-of-record
};

/*******************************************************************************
 Useful defines
*******************************************************************************/

inline LogConn::LogConn ()
{
	trace ("LogConn::LogConn");

	m_eor [0] = 0xA;			// ASCII LF (line feed)
	m_eor [1] = 0xD;			// ASCII CR (carrige return)
}

inline LogConn::~LogConn ()
{
	trace ("LogConn::~LogConn");
}

#endif /* LOGCONN_H */

