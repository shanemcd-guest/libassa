// -*- c++ -*-
//------------------------------------------------------------------------------
//                               MonitorConn.h
//------------------------------------------------------------------------------
// $Id: MonitorConn.h,v 1.2 2006/07/20 02:30:55 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// Created: Thu Apr 17 23:22:39 EDT 2003
//------------------------------------------------------------------------------
#ifndef MONITOR_CONN_H
#define MONITOR_CONN_H

#include <assa/ServiceHandler.h>
#include <assa/IPv4Socket.h>

using ASSA::ServiceHandler;
using ASSA::IPv4Socket;

class Conn;

/*******************************************************************************
 Class MonitorConn
*******************************************************************************/

class MonitorConn : public ServiceHandler<IPv4Socket>
{
public:
	MonitorConn (IPv4Socket* stream_);
	~MonitorConn ();

	virtual int open ();

	virtual int handle_read (int fd_);
	virtual int handle_close (int /* fd */);
	void notify (const char* msg_);

private:
	static const size_t MAXMSGLEN = 128;    // Maximum message length
	void parse_record ();
	void process_get_cmd (const std::string& token_);
	void process_list_cmd ();

private:
	u_int m_iolen;				// Number of bytes in I/O buffer so far
	char  m_iobuf [MAXMSGLEN];	// Message buffer
	Conn* m_current_conn;
	char m_eor [2];				// End-of-Record
};

/*******************************************************************************
 Useful defines
*******************************************************************************/

#endif /* MONITOR_CONN_H */

