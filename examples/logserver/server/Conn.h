// -*- c++ -*-
//------------------------------------------------------------------------------
//                               Conn.h
//------------------------------------------------------------------------------
// $Id: Conn.h,v 1.2 2006/07/20 02:30:55 vlg Exp $
//------------------------------------------------------------------------------
//  Copyright (c) 2002 by Vladislav Grinchenko
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version
//  2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
// Created: Thu Apr 17 23:22:39 EDT 2003
//------------------------------------------------------------------------------
#ifndef CONN_H
#define CONN_H

#include <assa/ServiceHandler.h>
#include <assa/IPv4Socket.h>
#include <assa/FileLogger.h>
#include <assa/Repository.h>
using ASSA::ServiceHandler;
using ASSA::IPv4Socket;

#include "LogServer-main.h"

class MonitorConn;

/*******************************************************************************
 Class Conn
*******************************************************************************/

class Conn : public ServiceHandler<IPv4Socket>
{
public:
	Conn (IPv4Socket* stream_);
	~Conn ();

	virtual int open ();

	virtual int handle_read (int fd_);
	virtual int handle_close (int /* fd */);

	const std::string& get_app_name () const { return m_app_name; }
	void subscribe (MonitorConn* mc_) { m_observers.push_back (mc_); }
	void unsubscribe (MonitorConn* mc_) { m_observers.erase (mc_); }

private:
	std::string wstate () const;
	void shift_logfile ();

private:
	enum msg_t { 
		SIGN_ON,				/**< Login message  */
		SIGN_OFF,				/**< Logout message */
		LOG_MSG					/**< Payload data   */
	};
	enum wstate_t {
		wait_for_header,		/**< Waiting for the message header */
		wait_for_signon,		/**< Waiting for SIGN_ON message    */
		wait_for_signoff,       /**< Waiting for SIGN_OFF message   */
		wait_for_logmsg         /**< Waiting for LOG_MSG message    */
	};
	enum state_t {
		opened,
		closed
	};

	wstate_t m_wstate;
	state_t m_state;
	
	/// Incoming message size
	int m_msg_size;

	/// Incoming message type
	int m_msg_type;

	/// Maximum logfile size can reach
	int m_maxsize;

	/// Logging application name
	std::string m_app_name;

	/// Logfile name
	std::string m_logfname;

	/// Logfile stream
	std::ofstream m_sink;

	/// Bytes written to the sink so far.
	u_long m_bytecount;

	/// Repository of all connected MonitorConn observers
	ASSA::Repository<MonitorConn> m_observers;
};

/*******************************************************************************
 Inline functions
*******************************************************************************/

inline
Conn::
Conn (IPv4Socket* stream_) : 
	ServiceHandler<IPv4Socket> (stream_),
	m_maxsize (1048576),
	m_bytecount (0),
	m_wstate (wait_for_header),
	m_state (closed)
{
	trace_with_mask ("Conn::Conn",LSVRTRACE);
	/* no-op */
}

inline
Conn::
~Conn ()
{
	trace_with_mask ("Conn::~Conn",LSVRTRACE);
}

inline std::string
Conn::
wstate () const
{
	return (m_wstate == wait_for_signon ? "wait_for_signon"   :
			m_wstate == wait_for_signoff ? "wait_for_signoff" :
			"wait_for_logmsg");
}

#endif /* CONN_H */

