// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: Assure.h,v 1.3 2007/05/14 19:19:50 vlg Exp $
//------------------------------------------------------------------------------
//                           Assure.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2000,2004,2005  Vladislav Grinchenko <vlg@users.sf.net>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef ASSURE_H
#define ASSURE_H

#include <unistd.h>
#include <errno.h>              /* errno */
#include <signal.h>             /* raise */

#include "assa/Logger.h"

// DO NOT PUT MACROS IN A NAMESPACE!

/** @file Assure.h

	A collection of assert function wrappers.
*/

/** @def Assure_exit(exp_)
	Macro that makes program exit if assert fails.

	assert a la ASSA. If expression exp_ is evaluated to false, error message
	is logged and current process is terminated with SIGTERM signal.
 
	@param exp_ expression to evaluate
*/

#define Assure_exit( exp_ ) \
    do { \
        if ( !(exp_) ) { \
			DL((ASSA::ASSAERR,"Assure Aborted False Expression!\n"));		\
			DL((ASSA::ASSAERR,"Error on line %d in file %s\n", __LINE__, __FILE__)); \
	       ::raise( SIGTERM ); \
        } \
    } while (0)


/** @def Assure_return(exp_)
	Test condition and return bool from a function if assertion fails
    
	Expression exp_ is evaluated and tested for the truth.
	If expression is false, error message with file name and line
	number is logged to the log file, and program control is returned back 
	from current execution scope with return value equal to FALSE.
	
	@param exp_ expression to evaluate
 
	@return FALSE if expression evaluates to false; if expression happens
	to evaluate to TRUE, Assure_return() DOES NOT return - program continues
	its natural execution flow.
 */

#define Assure_return(exp_) \
    do { \
        if ( !(exp_) ) { \
	        DL((ASSA::ASSAERR,"Assure Returned False Expression!\n"));		\
            DL((ASSA::ASSAERR,"Error on line %d in file %s\n", __LINE__, __FILE__)); \
	        return (false); \
        } \
    } while (0) 

/** @def Assure_return_void(exp_, value_)
	Test condition and return from a function immediately if assertion fails.
    
	Expression exp_ is evaluated and tested for the truth.
	If expression is false, error message with file name and line
	number is logged to the log file, and program control is returned back 
	from current execution scope.
	
	@param exp_ expression to evaluate
 */

#define Assure_return_void(exp_) \
    do { \
        if ( !(exp_) ) { \
	        DL((ASSA::ASSAERR,"Assure Returned False Expression!\n"));	\
            DL((ASSA::ASSAERR,"Error on line %d in file %s\n", __LINE__, __FILE__)); \
	        return; \
        } \
    } while (0) 

/** @def Assure_return_value(exp_, value_)
	Test condition and return value_ from a function if assertion fails.
    
	Expression exp_ is evaluated and tested for the truth.
	If expression is false, error message with file name and line
	number is logged to the log file, and program control is returned back 
	from current execution scope with return value equal to value_.
	
	@param exp_   expression to evaluate
	@param value_ value to return

 	@return value_ if expression evaluates to false; if expression happens
	to evaluate to TRUE, Assure_return_value() DOES NOT return - program 
	continues its natural execution flow.
 */

#define Assure_return_value(exp_,value_) \
    do { \
        if ( !(exp_) ) { \
	        DL((ASSA::ASSAERR,"Assure Returned False Expression!\n"));	\
            DL((ASSA::ASSAERR,"Error on line %d in file %s\n", __LINE__, __FILE__)); \
	        return (value_); \
        } \
    } while (0) 

#endif /* ASSURE_H */  
