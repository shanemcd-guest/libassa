// -*- c++ -*-
//------------------------------------------------------------------------------
//                             UDPSocket.C
//------------------------------------------------------------------------------
//  Copyright (c) 2000 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//
//------------------------------------------------------------------------------
//  Created: 03/22/99
//------------------------------------------------------------------------------

#include "assa/UDPSocket.h"

using namespace ASSA;

bool 
UDPSocket::
open (const int domain_)
{
	trace("UDPSocket::open");

	m_type = domain_;
	m_fd = ::socket (m_type, SOCK_DGRAM, 0);

	if (m_fd < 0) {
		setstate (Socket::failbit);
		return false;
	}
	clear ();
	return true;
}

bool 
UDPSocket::
close ()
{
	trace("UDPSocket::close()");
	if ( m_fd >= 0 ) {
		::close(m_fd);
		setstate (Socket::failbit);
		m_fd = -1;
	}
	return true;
}

bool 
UDPSocket::
bind (const Address& my_address_)
{
	trace("UDPSocket::bind");

	int ret = ::bind (m_fd, (SA*) my_address_.getAddress(),
			  my_address_.getLength());
	if (ret < 0) {
		setstate (Socket::failbit);
		return false;
	}
	return true;
}
    


