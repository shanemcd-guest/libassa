// -*- c++ -*-
//------------------------------------------------------------------------------
//                             Repository.h
//------------------------------------------------------------------------------
//  Copyright (c) 2003 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Date: May 7, 2003
//------------------------------------------------------------------------------

#ifndef REPOSITORY_H
#define REPOSITORY_H

#include <vector>
using std::vector;

namespace ASSA {

/** @file Repository.h

    Repository class is a template class to hold a collection of 
    similar objects. It is typically used to hold references dynamically
    created ServiceHandlers. Repository doesn't manage references it holds.
    It is only used for the purposes of sending a signal to all elements
    of the Repository collectively.
*/

	template<typename T>
	class Repository 
	{
	public:
		typedef T* value_type;
		typedef size_t size_type;

		typedef typename std::vector<value_type> list_t;
		typedef typename std::vector<value_type>::iterator iterator;
		typedef typename std::vector<value_type>::const_iterator const_iterator;

		/** Constructor 
		 */
		Repository () {
			m_collection = new list_t; 
		}

		/// Destructor
		virtual ~Repository () {
			if (m_collection) {	clear (); delete m_collection; }
		}

		/// Get iterator to the first element of the repository
		iterator begin () { return m_collection->begin (); }

		/// Get constant iterator to the first element of the repository
		const_iterator begin () const { return m_collection->begin (); }

		/// Get iterator to the end of the repository
		iterator end () { return m_collection->end (); }

		/// Get constant iterator to the end of the repository
		const_iterator end () const { return m_collection->end (); }

		/// Return true if repository is empty
		bool empty () const { return m_collection->empty (); }

		/// Return number of elements in the repository 
		size_type size () const { return m_collection->size (); }

		/// Add new element to the repository
		void push_back (const value_type& x_) { m_collection->push_back (x_); }

		/// Remove element at the position_ iterator
		void erase (iterator position_) { m_collection->erase (position_); }

		/** Remove element
			@return true if element was found and erased; false otherwise
		*/
		bool erase (const value_type& x_) {
			iterator it = begin ();
			while (it != end ()) {
				if ((*it) == x_) { erase (it); break; }
				it++;
			}
		}

		/// Empty repository
		void clear () {
			m_collection->erase (m_collection->begin (), m_collection->end ());
		}

	private:
		list_t* m_collection;
	};
}  // @end namespace ASSA

#endif /* REPOSITORY_H */  
