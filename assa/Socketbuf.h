// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: Socketbuf.h,v 1.3 2005/10/08 02:42:00 vlg Exp $
//------------------------------------------------------------------------------
//                              Socketbuf.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef SOCKET_BUF_H
#define SOCKET_BUF_H

#include "assa/Streambuf.h"

namespace ASSA {

/** @file Socketbuf.h 
 *
 * Socketbuf class is derived from Streambuf to provide I/O with
 * the socket file descriptor.
 */

class Socket;

class Socketbuf : public Streambuf
{
public:
	Socketbuf (Socket* s_);
	virtual ~Socketbuf ();

protected:
	virtual int  sync ();
	virtual int  underflow ();
	virtual int  overflow (int c_ = EOF);
	virtual int  showmanyc ();
	virtual int  doallocate ();

private:
	int  flush_output ();
	int  sys_read  (char* b_, int len_);
	int  sys_write (char* b_, int len_);
	void xput_char (char c_);

private:
	/** Reference to the Socket stream.
	 */
	Socket* m_s;
};

} // end namespace ASSA

#endif /* SOCKET_BUF_H */  
