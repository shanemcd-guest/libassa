// -*- c++ -*-
//------------------------------------------------------------------------------
//                         UDPSocket.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999,2006 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 03/22/99
//------------------------------------------------------------------------------
#ifndef UDP_SOCKET_H
#define UDP_SOCKET_H

#include "assa/Socket.h"

namespace ASSA {

/** @file UDPSocket.h
 *
 * Class UDPSocket is an implementation of
 * UNIX domain socket that is the base class for more specialized
 * domain socket classes.
 */

class UDPSocket : public Socket {
public:
	/// Default constructor
	UDPSocket ()
	{
		trace("UDPSocket::UDPSocket()");
	}

	/** Constructor
	 *  @param fd_ file descriptor to use
	 */
	UDPSocket (const handler_t fd_)
	{
		trace("UDPSocket::UDPSocket(fd)");
		m_fd = fd_;
	}

	/** Destructor will close connection */
	virtual ~UDPSocket ()
	{
		trace("UDPSocket::~UDPSocket");
	}

	/** Create socket. Socket domain type is specified as
	 *  AF_INET for internet socket and AF_UNIX for UNIX domain socket
	 *  (full duplex pipe).
	 *
	 *  @param domain_ domain
	 *  @return true if socket is created successfully, false otherwise
	 */
	bool open (const int domain_);

	/** Close socket connection.
	 *  @return true if success, fail if call to close() failed.
	 */
	bool close ();

	/** Server in UDP client-server scenario has to bind socket
	 *  to its local well-known port. This is the same bind call as
	 *  in IPv4 - maybe it should be generalized in parent class.
	 *
	 *  @param my_address_ address to bind to
	 */
	bool bind (const Address& my_address_);

	/// Get socket file descriptor
	handler_t getHandler() const  { return m_fd; }

	/// Get socket domain type
	const int getDomain() const { return m_type; }

protected:
	/// Set file descriptor
	void setHandler(const int fd_) { m_fd = fd_; }

	/// Set socket domain type
	void setDomain(const int type_) { m_type = type_; }
};

} // end namespace ASSA

#endif // UDP_SOCKET_H


