// -*- c++ -*-
//------------------------------------------------------------------------------
//                            INETAddress.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef INET_ADDRESS_H
#define INET_ADDRESS_H

#include <vector>
using std::vector;

#include "assa/Address.h"

namespace ASSA {

/** @file INETAddress.h 

An incapsulation of TCP/UDP Internet Protocol socket address structure.
*/

class INETAddress : public Address {
public:
	/** @enum Protocol */
	enum Protocol { 
		TCP,					/**< TCP protocol */
		UDP						/**< UDP protocol */
	};

public:
	/// Default constructor.
	INETAddress ();

	/** Constructor to create address on a client side given
	 *  address in struct in_addr and integer port number.
	 *
	 *  @param haddr_ XDR-encoded server host address structure
	 *  @param port_ Server listening port
	 */
	INETAddress (struct in_addr * haddr_, int port_);

	/** Constructor to create address on the client side given
	 *  host name and integer port number.
	 *
	 *  @param host_ server host name
	 *  @param port_ port server listens on
	 */
	INETAddress(const char* host_, int port_);

	/** Constructor to create address on the client side given
	 *  host name and service name (as in /etc/services) and
	 *  optionally protocol type.
	 *
	 *  @param host_ Server host name
	 *  @param service_ Server listening port
	 *  @param protocol_ protocol: TCP (default) or UDP
	 */
	INETAddress (const char* host_, const char* service_, 
				 Protocol protocol_ = TCP);

	/** Constructor to create address of listening socket on a
	 *  server side from integer port number.
	 *
	 *  @param port_ port to use
	 */
	INETAddress (int port_);

	/** Constructor to create address both client- and server-side
	    addresses. Address is derived from the character string address_
	    which can be specified in one of the following formats:

		Formats:
	       - [host:]service 
	       - service[@@host]

	    If host is omitted, wildcard (INADDR_ANY) address is
	    assumed. This essentially creates an address of the server's
	    listening socket. 

	    To create client-side address, use localhost (or host) name instead.

	    service entry can be either port name as listed in /etc/services or
	    integer port value. 
	 
	    @param address_ Address string.
	    @param protocol_ Protocol: TCP (by default) or UDP.
	 */
	INETAddress (const char* address_, Protocol protocol_ = TCP);

	/// Copy constructor
	INETAddress (SA_IN* address_);

	/// Copy constructor from base address
	INETAddress (SA* address_);

	/// Destructor
	~INETAddress () { 
//		trace_with_mask("INETAddress::~INETAddress",SOCKTRACE);
	}

	/// Return address length
	const int getLength () const { return sizeof (m_address); }

	/// Get hold of address structure
	SA* getAddress () const { return (SA*) &m_address; }

	/// Return host name
	string getHostName ();

	/// Return port
	int getPort () const { return ntohs (m_address.sin_port); }

	/// Dump the address content to log file
	void dump ();

	/** Return fully-qualified host name. Note that a host
		can have name aliases. If it does, they are returned
		via argument.

		@param aliases_ List of host aliases, if any
		@return fully-qualified host name.
	*/
	static string 
	get_fully_qualified_domain_name (vector<string>& aliases_);

private:
	/** Makes socket address out of host name and port.
	    Host name is either a host name, or an IPv4 address in 
	    standard dot notation, or an IPv6 address in colon 
	    (and possibly dot) notation. If it is in dot notation,
	    no lookup is performed. 

	    Otherwise, lookup is performed by consulting name resolution 
	    services in order specified in in /etc/host.conf file 
	    (named(8) first, then /etc/hosts, and so on).

	    Port port_ must be supplied in network-independent byte order.
	    If host_ is an empty string, then local host name is assumed.

	    If failed, state of the object is set to bad, and errno 
	    indicates the error occured.
	 */
	void createHostPort (const char* host_, int port_);

	/** Lookup port by its service name found in /etc/services.
	   serv_ is either service name, or integer port number.
	   If it is integer port number, it is converted to 
	   the network-independent byte order and no lookup is performed.
	 
	   @param serv_ Service name.
	   @param prot_ Protocol: tcp (default) or udp.
	   @return Port number in the network-independent byte order,
	   or 0 if lookup failed.
	 */
	int getServiceByName (string serv_, Protocol prot_ = TCP);

	/** Perform initialization common to all ctors.
	 */
	void init ();

private:
	/// Cached fully-qualified domain name
	static string m_fqdn_cache;

private:
	/// Internet address structure sockaddr_in
	SA_IN m_address;
};


} // end namespace ASSA

#endif /* INET_ADDRESS_H */
