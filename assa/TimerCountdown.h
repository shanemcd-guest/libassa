// -*- c++ -*-
//------------------------------------------------------------------------------
//                        TimerCountdown.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 08/25/1999
//------------------------------------------------------------------------------
#ifndef TIMER_COUNTDOWN_H
#define TIMER_COUNTDOWN_H

#include "assa/TimeVal.h"

namespace ASSA {

/** @file TimerCountdown.h 
 *
 * TimerCountdown class keep the track of the elapsed time. This class 
 * substracts the time elapsed between instantiation and destruction time from
 * argument timeval passed to the constructor.
 */

class TimerCountdown
{
public:
	/** Constructor. 
	 */
	TimerCountdown (TimeVal* wait_time_);

	/** Destructor.
	 */
	~TimerCountdown ();

private:
	/// Maximum time to wait
	TimeVal* m_maxWaitTime;

	/// Time when countdown started
	TimeVal  m_start;
};

//------------------------------------------------------------------------------
// inlines
//------------------------------------------------------------------------------

inline
TimerCountdown::
TimerCountdown (TimeVal* wt_)
	: m_maxWaitTime (wt_), m_start (TimeVal::gettimeofday ())
{
}

inline
TimerCountdown::
~TimerCountdown ()
{
	if (m_maxWaitTime == NULL)
		return; 

	TimeVal elapsed (TimeVal::gettimeofday ());
	elapsed -= m_start;

	if ( *m_maxWaitTime > elapsed )
		*m_maxWaitTime -= elapsed;
	else
		*m_maxWaitTime = TimeVal::zeroTime ();
}

} // end namespace ASSA

#endif /* TIMER_COUNTDOWN_H */  
