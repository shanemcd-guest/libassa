// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: CommonUtils.cpp,v 1.10 2006/07/20 02:30:53 vlg Exp $
//------------------------------------------------------------------------------
//                          CommonUtils.cpp
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2003  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include <iomanip>
#include <string.h>

#include <stdlib.h>

#include <ctype.h>
#include <unistd.h>				// getcwd(3)

#ifdef WIN32
static char *home_dir = ".";	/* we feel (no|every)where at home */
#else
#    include <pwd.h>
#endif

#include "assa/Logger.h"
#include "assa/CommonUtils.h"

void
ASSA::Utils::
split (const char* src_, std::vector<std::string>& vec_)
{
    std::istringstream input (src_);
    vec_.erase (vec_.begin (), vec_.end ());

    std::string token;
    while (input >> token) {
		vec_.push_back (token);
    }
}

int
ASSA::Utils::
split_pair (const string& text_, char sep_, string& lhs_, string& rhs_)
{
    int pos = 0;
    if ((pos = text_.find (sep_)) == string::npos) {
        return -1;
    }
    lhs_ = text_.substr (0, pos);
    rhs_ = text_.substr (pos+1, text_.size ());
    pos = rhs_.size () -1;
    if (rhs_[0] == '"' || rhs_[0] == '\'') {
		rhs_[0] = ' ';
	}
    if (rhs_[pos] == '"' || rhs_[pos] == '\'') {
		rhs_[pos] = ' ';
	}
    return 0;
}

int
ASSA::Utils::
ltrim (std::string& text_, const std::string& delim_)
{
    std::string::size_type idx;
    idx = text_.find_first_of (delim_);
    if (idx != std::string::npos) {
		text_.replace (0, idx+1, "");
		return 0;
    }
    return -1;
}

int
ASSA::Utils::
rtrim (std::string& text_, const std::string& delim_)
{
    std::string::size_type idx;
    idx = text_.find_last_of (delim_);
    if (idx != std::string::npos) {
		text_.replace (idx, text_.size (), "");
		return 0;
    }
    return -1;
}

void
ASSA::Utils::
trim_sides (std::string& text_)
{
    std::string::size_type idx;

	idx = text_.find_first_not_of (" \t");
    if (idx != std::string::npos) {
		text_.replace (0, idx, "");
	}

	idx = text_.find_last_not_of (" \t");
    if (idx != std::string::npos) {
		text_.replace (idx + 1, text_.size (), "");
	}
}

void
ASSA::Utils::
find_and_replace_char (std::string& text_, char src_, char dest_)
{
	string::iterator pos = text_.begin ();
	while (pos != text_.end ()) {
		if ((*pos) == src_) {
			(*pos) = dest_;
		}
		pos++;
	}
}

std::string
ASSA::Utils::
strenv (const char* in)
{
    char b [1024];
    char* ret = b;
    char* r = ret;

    if (*in == '~') {			//  '~' OR '~/'
		if ( *(in+1) == 0 || *(in+1) == '/' ) {
			in++;
			strcpy (ret, getenv ("HOME") ? getenv ("HOME") : "");
			r += strlen (ret);
		}
		else {
			in++;
			char lname [256];
			char* lp = lname;
			const char* sp = strchr (in, '/'); // find first '/' in string
			if ( sp ) {
				while (in != sp) *lp++ = *in++;
				*lp = 0;
			}
			else {
				while (*in) *lp++ = *in++;
				*lp = 0;
			}
#ifdef WIN32
            strcpy (ret, home_dir);
            r += strlen (ret);
#else
			// lookup user's home directory in /etc/passwd file
			struct passwd* p = getpwnam (lname); 
			if ( p ) {
				strcpy (ret, p->pw_dir ? p->pw_dir : "");
				r += strlen (ret);
			}
#endif
		}
    }

    while (*in) {
		if (*in == '$') {
			char varname [80];
			if (*++in == '(') {       
				++in;
				const char *end = strchr (in,')');
				if (!end)
					break;
				strncpy (varname, in, end-in);
				varname [end-in] = '\0';
				in = end+1;
			}
			else if (*in == '{') {
				const char *end = strchr (in,'}');
				if (!end)
					break;
				strncpy (varname, in, end-in);
				varname [end-in] = '\0';
				in = end+1;
			}
			else {       
				char* vp = varname;
				while (isalnum (*in) || *in == '_' ) { // letter OR digit
					*vp++ = *in++;
				}
				*vp = '\0';
			}
			char* ep = ::getenv (varname);
			while (ep && *ep) *r++ = *ep++;
			continue;
		}
		else if (*in == '\\' && *(in+1)) {
			in++;  // allow escaped dollar signs
		}
		*r++ = *in++;
    }
    *r = '\0';
    return ret;
} 

std::string
ASSA::Utils::
get_cwd_name (void)
{
    std::string ret;
    int size = 256;
    char* chr_ptr = 0;

    while (true) {
	chr_ptr = new char [size];
	if (::getcwd (chr_ptr, size-1) != NULL) {
	    ret = chr_ptr;
	    delete [] chr_ptr;
	    return ret;
	}
	if (errno != ERANGE) {
	    return ret;		// Any error other then a path name too long
	    // for the buffer is bad news.
	}
	delete [] chr_ptr;
	size += 256;
    }
}
