// -*- c++ -*-
//------------------------------------------------------------------------------
//                               FdSet.cpp
//------------------------------------------------------------------------------
//  Copyright (C) 2006  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------

#include "FdSet.h"
#include "Logger.h"

using namespace ASSA;

bool
FdSet::
setFd (handler_t fd_) 
{ 
	FD_SET (fd_, this); 

#if !defined (WIN32)
	ActiveFDs_Iter iter;
	iter = std::find (m_actfds.begin (), 
					  m_actfds.end (), 
					  fd_);
	if (iter == m_actfds.end ()) { // not found
		m_actfds.push_back (fd_);
	}
#endif

	return true;
}

bool 
FdSet::
clear (handler_t fd_) 
{
	DL ((REACT,"Clearing fd=%d\n", fd_));

	if (!isSet (fd_)) {
		DL ((REACT,"Not set! - ignoring.\n"));
		return false;
	}

	FD_CLR (fd_, this); 
	if (FD_ISSET (fd_, this)) {
		DL ((REACT,"Woop - an error! FD_CLR failed!\n"));
	}

#if !defined (WIN32)
	ActiveFDs_Iter iter;
	iter = std::find (m_actfds.begin (), 
					  m_actfds.end (), 
					  fd_);
	if (iter != m_actfds.end ()) { 
		DL ((REACT,"fd=%d found and erased\n", fd_));
		m_actfds.erase (iter);
	}
	else {
		DL ((REACT,"fd=%d not found in m_actfds list!\n", fd_));
	}
#endif

	return true;
}

void 
FdSet::
sync () 
{ 
#if !defined (WIN32)
	ActiveFDs_Iter iter;
  restart:
	iter = m_actfds.begin ();
	while (iter != m_actfds.end ()) {
		if (!isSet (*iter)) {
			m_actfds.erase (iter);
			goto restart;
		}
		iter++;
	}
#endif
}

void 
FdSet::
reset () 
{ 
	::memset(this, 0, sizeof (fd_set)); 

#if !defined (WIN32)
	m_actfds.clear ();
#endif
}

int
FdSet::
maxInSet ()
{
#if defined (WIN32)
	return 0;					// win32 select doesn't need this value
#else
	if (m_actfds.size () == 0) {
		return 0;
	}
	ActiveFDs_Iter iter = std::max_element (m_actfds.begin (), m_actfds.end ());
	return (*iter);
#endif
}

std::string
FdSet::
dump_c_str ()
{
	std::ostringstream report;

	report << " enabled=" << numSet ();

#if defined (WIN32)
	if (this->fd_count) {
		report << " : ";
	}
	for (int i=0; i < this->fd_count; i++) {
		report << " " << this->fd_array[i];
	}
#else /* UNIX */
	ActiveFDs_Iter iter = m_actfds.begin ();
	if (m_actfds.size ()) {
		report << " : ";
	}
	while (iter != m_actfds.end ()) {
		report << " " << (u_int)*iter;
		iter++;
	}
#endif

	report << std::ends;
	return (report.str ());
}

