// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: CommonUtils.h,v 1.8 2006/07/20 02:30:53 vlg Exp $
//------------------------------------------------------------------------------
//                             CommonUtils.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2003,2005  Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef COMMON_UTILS_H
#define COMMON_UTILS_H

#include <sstream>

#include <string> 
#include <vector>
using std::vector;
using std::string;

/** CommonUtils.h

   A random collection of unrelated static functions.
*/


/** Windows has its own wicked way of delimiting path.
 *  This is *borrowed* fro glib.
 */

#if defined (WIN32)

#include <Windows.h>

#define ASSA_DIR_SEPARATOR          '\\'
#define ASSA_DIR_SEPARATOR_S        "\\"
#define ASSA_IS_DIR_SEPARATOR(c)    ((c) == ASSA_DIR_SEPARATOR || (c) == '/')
#define ASSA_SEARCHPATH_SEPARATOR   ';'
#define ASSA_SEARCHPATH_SEPARATOR_S ";"

#else  /* POSIX */

#define ASSA_DIR_SEPARATOR          '/'
#define ASSA_DIR_SEPARATOR_S        "/"
#define ASSA_IS_DIR_SEPARATOR(c)    ((c) == ASSA_DIR_SEPARATOR)
#define ASSA_SEARCHPATH_SEPARATOR   ':'
#define ASSA_SEARCHPATH_SEPARATOR_S ":"

#endif 

namespace ASSA {
namespace Utils {

    /** 
	Split character string into tokens separated by the whitespace
	character (blank, tab, newline, formfeed, and carriage return).
	The vec_ vector is emptied out prior parsing string text_.
	
	@param text_  string of tokens to split
	@param vec_  vector with tokens extracted from the string str_
    */
    void split (const char* text_, std::vector<std::string>& vec_);

    /**
       Split input string into two parts separated by the separator 
       character.
       
       @param text_  Input string to split
       @param sep_   Separator character
       @param lhs_   Return left-hand side of the input string
       @param rhs_   Return right-hand side of the input string
       @return 0 on success; -1 if separator character was not found.
    */
    int split_pair (const string& text_, char sep_, string& lhs_, string& rhs_);

    /**
       Trim string from the beginning to the left of the delimiter.
       Delimiter is removed as well.
     
       @param text_   String to modify
       @param delim_  Delimiter character
       @return 0 on success; -1 on error
     */
    int ltrim (std::string& text_, const std::string& delim_);

    /**
       Trim string from the delimiter to the end of the string.
       Delimiter is removed as well.

       @param text_    String to modify
       @param delim_   Delimiter character
       @return 0 on success; -1 on error

     */
    int rtrim (std::string& text_, const std::string& delim_);

	/** 
		Trim white spaces and tabs from the beginning and the end of
		the text string.

		@param text_ String to trim
	*/
	void trim_sides (std::string& text_);

	/**
	   Find and relpace all instances of src_ character
	   with dest_ character in a string text_.

	   @param text_ String to modify
	   @param src_  Find the character
	   @param dest_ Character to replace with
	*/
	void find_and_replace_char (std::string& text_, char src_, char dest_);

    /** 
     * Expand the passed string in_ by substituting environment
     * variable names for their values. Environment variables must be 
     * preceeded by dollar sign and optionally enclosed in parentheses:
     * $ENV_NAME, or $(ENV_NAME), or ${ENV_NAME}. $HOME is equivalent
     * to '~' or '~username'. If later is used, "username" is looked up
     * in the password file.
     */
    std::string strenv (const char* in_);

    /**
     * Get current working directory. 
     *
     * @return the current working directory on success, and an empty
     * string on failure with errno set to indicate the error occured.
     */
    std::string get_cwd_name ();

	/**
	 * Portable sleep
	 *
	 * @param secs_to_sleep_ Number of seconds to sleep
	 */
	inline void sleep_for_seconds (long secs_to_sleep_) 
	{
#if defined (WIN32)
		SleepEx (secs_to_sleep_ * 1000, FALSE);
#else
		::sleep (secs_to_sleep_);
#endif
	}


} // end namespace Utils
} // end namespace ASSA

#endif /* COMMON_UTILS_H */  
