// -*- c++ -*-
//------------------------------------------------------------------------------
//                             Regexp.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2003  Vladislav Grinchenko <vlg@users.sourceforge.net>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef REGEXP_H
#define REGEXP_H

#include "assa/Assure.h"
#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <regex.h>

#ifdef __cplusplus
}
#endif	/* C++ */


#include <string>

namespace ASSA {

/** @file Regexp.h 

    Wrapper class for UNIX regexp (3).
*/
/** Regexp class.

    Class Regexp wraps regexp structure and associated library
    functions.
*/

    class Regexp {
    public:
		/** Constructor.
			@param pattern_ Regular expression pattern
		*/
		Regexp (const std::string& pattern_);

		/** Destructor.
			Release all allocated resources.
		*/
		~Regexp ();

		/** Match an ASCII character string agains the pattern this class wraps.

			@param text_ Input text to match against the pattern.
			@return 0 if text_ matches the pattern; -1 if not.
		*/
		int match (const char* text_);

		/** Return error message
		 */
		const char* get_error () const { return m_error_msg; }

		/** Return the original pattern (uncompiled)
		 */
		const char* get_pattern () const { return m_pattern; }

    private:
		char*      m_pattern;
		char*      m_error_msg;
		regex_t*   m_compiled_pattern;
    };
} // @end namespace

#endif /* REGEXP_H */



