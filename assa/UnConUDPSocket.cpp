// -*- c++ -*-
//------------------------------------------------------------------------------
//                         UnConUDPSocket.C
//------------------------------------------------------------------------------
//  Copyright (c) 1999 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 03/23/99
//------------------------------------------------------------------------------

#include "assa/UnConUDPSocket.h"

#if defined (WIN32)
typedef unsigned int socklen_t;
#endif

using namespace ASSA;

int 
UnConUDPSocket::
recvfrom (char* buf_, int size_, Address* peer_addr_)
{
	// ::recvfrom() can return 0 bytes which is not 
	// considered an eof. Peer can advertise its address to 
	// the server by sending 0 bytes length message.
	//

	// char self[] = "Socket::recvfro"; trace(self);

	// Setting saddr_len is crucial to proper ::recvfrom() operation.
	// If left improprely initialized, ::recvfrom() won't fill in peer's 
	// address and won't report an error either. If SA ptr is passed to 
	// recvfrom() along with uninitialized address len (or set to 0), 
	// recvfrom() returns zeroed out address structure!!!

	int len;
	socklen_t pa_len = peer_addr_->getLength();

	SA* pa = peer_addr_->getAddress();

#if defined (__CYGWIN32__) || defined (WIN32)
	len = ::recvfrom(getHandler(), buf_, size_, 0, pa, (int*)&pa_len);
#else  // posix/unix
	len = ::recvfrom(getHandler(), buf_, size_, 0, pa, &pa_len);
#endif

	// Q: for UNIX domain socket, returned length will be essential to
	// remember and probably should be set in peer_addr_ by calling
	// setLength().....
	
	return len;
}

int 
UnConUDPSocket::
sendto (const char* buf_, const unsigned int size_, const Address* peer_addr_)
{
	return  ::sendto (getHandler(), buf_, size_, 0,
			 peer_addr_->getAddress(),
			 peer_addr_->getLength());
}


