// -*- c++ -*-
//------------------------------------------------------------------------------
//                        PriorityQueue_Impl.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 09/17/1999
//------------------------------------------------------------------------------

#ifndef PRIORITY_QUEUE_IMPL_H
#define PRIORITY_QUEUE_IMPL_H

namespace ASSA {

/** @file PriorityQueue_Impl.h 
	Interface class that defines Implementor of the Bridge pattern.
*/

// less<> is borrowed from STL implementation.

/** @struct Bfunc
	Bfunc is used by PriorityQueue_impl.
*/
template <class Arg1, class Arg2, class Result>
struct Bfunc {
	typedef Arg1   first_argument_type;
	typedef Arg2   second_argument_type;
	typedef Result result_type;
};
 

/** @struct Less
	Less is  borrowed from STL implementation.
*/
template <class T>
struct Less : public Bfunc<T, T, bool> {
	bool operator()(const T& x, const T& y) const { return x < y; }
};


/** Class PriorityQueue_Impl.

    Interface class that defines Implementor of the Bridge pattern.
    Derived classes provide concrete implementations of PriorityQueue.
*/

template< class T, class Compare >
class PriorityQueue_Impl
{
public:
	virtual ~PriorityQueue_Impl ();

	virtual void     insert (const T&) =0;
	virtual T        pop () =0;
	virtual const T& top () const =0;
	virtual bool     remove (T) =0;
	virtual size_t   size () =0;
	virtual T&       operator[] (int) =0;
};

template<class T, class Compare>
inline
PriorityQueue_Impl<T, Compare>::
~PriorityQueue_Impl () 
{ 
	// Here is the twist: we must provide a definition for the virtual
	// destructor. We need the definition here because the way virtual 
	// destructors work. Most derived class's destructor is called first,
	// then the destructor of each base class is called. That means that
	// the compiler will generate a call to ~PriorityQueue_Impl, even
	// though the class is abstract - that's why we need body here.
	
	/* no-op */ 
}

} // end namespace ASSA

#endif /* PRIORITY_QUEUE_IMPL_H */  
