// -*- c++ -*-
//------------------------------------------------------------------------------
//                           ConUDPSocket.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef CONUDP_SOCKET_H
#define CONUDP_SOCKET_H

#include "assa/UDPSocket.h"

namespace ASSA {

/** @file ConUDPSocket.h

    Encapsulation of a connected UDP socket.
*/

class ConUDPSocket : public UDPSocket {
public:
	/// Constructor
	ConUDPSocket () : UDPSocket() {
		char self[] = "ConUDPSocket::ConUDPSocket"; trace(self);
	}
    
	/// Destructor
	virtual ~ConUDPSocket () {
		char self[] = "ConUDPSocket::~ConUDPSocket"; trace(self);
	} 

	/** Connect socket to the peer.
	 *  @param peer_addr_ peer address
	 */
	bool connect (const Address& peer_addr_);

	/// Unconnect connected socket
	void unconnect ();

	/** Read specified number of bytes off the socket.
	 *  This function cannot be moved up in class hierarchy
	 *  because IPv4 read() is *very* different from UDP read (one
	 *  time shot).
	 *
	 *  @param buf_ buffer to save received data into
	 *  @param size_ expected packet size
	 *  @return number of bytes read or -1 on error, indicating 
	 *          the reason in errno. Packets of 0 size are possible.
	 */
	int read (char* buf_, const unsigned int size_);
	
	/** Perform blocking write by writing packet of specified size.
	 *
	 *  @param buf_ buffer to send
	 *  @param size_ packet size
	 */
	int write (const char* buf_ = NULL, const unsigned int size_ = 0);

	virtual int in_avail () const { return 0; }
};

} // end namespace ASSA

#endif // CONUDP_SOCKET_H



