// -*- c++ -*-
//------------------------------------------------------------------------------
//                                 MemDump.h
//------------------------------------------------------------------------------
//  Copyright (c) 1995 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//
//  Permission to use, copy, modify, and distribute this software      
//  and its documentation for any purpose and without fee is hereby     
//  granted, provided that the above copyright notice appear in all     
//  copies.  The author makes no representations about the suitability  
//  of this software for any purpose.  It is provided "as is" without   
//  express or implied warranty.
//------------------------------------------------------------------------------
// Creation Date: November 25, 1995
//------------------------------------------------------------------------------
#ifndef MEM_DUMP_H
#define MEM_DUMP_H

#include <stdio.h>
#include <string.h>

namespace ASSA {

/** @file MemDump.h

	A Hex/Ascii memory dump of similar to od(1) UNIX utility.

	A class that converts raw binary memory image into hex-ascii
	representation, much like od(1) does.

	MemDump transforms binary chunk of memory into corresponding
	hex and ascii formats.
*/

class MemDump 
{
private:
	/// pointer to converted image
	char* m_dump;

	/// static Null string
	static const char m_empty_str[];
	
public:
	/** Constructor converts original binary image to hex and ascii 
	    representation, and stores resultant image in internal buffer for
	    later retrieval. MemDump owns the image and will free the memory
	    once it is out of scope
	    
	    @param msg_  char pointer to original binary image.
	    @param len_  lenght of the binary image.
	*/
	MemDump (const char* msg_, int len_);

	/// Destructor releases image memory. 
	~MemDump();

	/** Obtain a pointer to the dump image (null-terminated char string).
	    @return pointer to converted memory area
	*/
	const char* getMemDump() const;

	/** Write hex/ascii dump of a memory region to log file.
	 *
	 * @param mask_ Debug mask as defined in debug.h
	 * @param info_ Information string to annotate the hex memory dump
	 * @param msg_  Pointer to the memory area
	 * @param len_  Number of bytes
	 */
	static void dump_to_log (unsigned long mask_, 
							 const char* info_, 
							 const char* msg_, 
							 int len_);
};


inline 
MemDump::
~MemDump()
{
	if (m_dump && m_dump != m_empty_str) {
		delete [] m_dump;
	}
	m_dump = NULL;
}

inline const char*
MemDump::
getMemDump () const
{
	return (m_dump ? (const char*) m_dump : m_empty_str);
}

} // end namespace ASSA
	
#endif /* MEM_DUMP_H */
