// -*- c++ -*-
//------------------------------------------------------------------------
//                              ConUDPSocket.cpp
//------------------------------------------------------------------------------
//  Copyright (C) 1999  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------

#ifndef WIN32

#include "assa/ConUDPSocket.h"
#include "assa/UNIXAddress.h"
#include "assa/INETAddress.h"

using namespace ASSA;

bool
ConUDPSocket::
connect (const Address& peer_address_)
{
	char self[] = "ConUDPSocket::connect"; trace(self);

	if ( ::connect (getHandler(),peer_address_.getAddress(),
					peer_address_.getLength()) < 0 ) {
		setstate (Socket::failbit);
		return false;
	}
	return true;
}

void
ConUDPSocket::
unconnect()
{
	// Ignore errors here. On some systems connect() might return
	// EAFNOSUPPORT error, on some might not, but it is OK.
	//
	char self[] = "ConUDPSocket::unconnect"; trace(self);

	if ( getDomain() == AF_INET ) {
		INETAddress addr;
		SA_IN* addrp = (SA_IN*) addr.getAddress();

		addrp->sin_family = AF_UNSPEC;
		(void) connect(addr);
	}
	else {			// AF_LOCAL
		// I haven't tested whether it works at all.
		
		UNIXAddress addr("");
		SA_UN* addrp = (SA_UN*) addr.getAddress();

		addrp->sun_family = AF_UNSPEC;
		(void) connect(addr);		
	}
}

int
ConUDPSocket::
read(char* packet_, const unsigned int size_)
{
	int len;
	len = ::read(getHandler(), packet_, size_);

	if (len == -1) {
		setstate (Socket::failbit);
	}
	else if ( len == 0 ) {
		setstate (Socket::failbit | Socket::eofbit);
	}
	return len;
}

int
ConUDPSocket::
write (const char* packet_, const unsigned int size_)
{
	return  ::write(getHandler(), (const void*) packet_, size_);
}

#endif // WIN32
