// -*- c++ -*-
//------------------------------------------------------------------------------
//                        UnConUDPSocket.h
//------------------------------------------------------------------------------
//  Copyright (c) 1999 by Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 04/01/99
//------------------------------------------------------------------------------
#ifndef UNCONUPD_SOCKET_H
#define UNCONUPD_SOCKET_H

#include "assa/UDPSocket.h"

namespace ASSA {

/** @file UnConUDPSocket.h 
 *
 * Class UnConUPDSocket class is unconnected UDP socket.
 */

class UnConUDPSocket : public UDPSocket {
public:
	/// Constructor.
	UnConUDPSocket();
    
	/// Destructor.
	~UnConUDPSocket();

	/** recvfrom() function receives a message from connectionless-mode
	 *  socket. It also permits the application to retrieve the
	 *  source address of received data.
	 *
	 *  @param buf_ points to buffer where the message should be stored
	 *  @param size_ buffer length is bytes
	 *  @param peer_addr_ pointer to the address structure where
	 *                    sending address is to be stored
	 *  @return length of the peer address
	 */
	int recvfrom (char* buf_, int size_, Address* peer_addr_);
	
	/** sendto() function sends a message through connectionless-mode
	 *  socket. The message will be sent to the address specified
	 *  by dest_addr_.
	 *
	 *  @param buf_ pointer to the buffer containing the message to
	 *              be sent
	 *  @param size_ the size of the message in bytes
	 *  @param dest_addr_ contains destination address
	 *  @return upon successful completion, number of bytes sent.
	 *          Otherwise, -1.
	 */
	int sendto (const char* buf_, 
				const unsigned int size_, 
				const Address* dest_addr_);

	/** This function returns the number of characters  
	 *  immediately  available in the get area of the underlying
	 *  Socketbuf buffer without making a system call.
	 *	Always return 0.
	 */
	virtual int in_avail () const { return 0; }
};

UnConUDPSocket::
UnConUDPSocket () : UDPSocket() 
{
	trace_with_mask ("UnConUDPSocket::UnConUDPSocket", SOCKTRACE);
}
    
UnConUDPSocket::
~UnConUDPSocket() 
{
	trace_with_mask ("UnConUDPSocket::~UnConUDPSocket", SOCKTRACE);
}

} // end namespace ASSA

#endif // UNCONUPD_SOCKET_H



