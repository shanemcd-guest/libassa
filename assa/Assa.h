// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: Assa.h,v 1.4 2005/10/08 02:42:00 vlg Exp $
//------------------------------------------------------------------------------
//                               Assa.h 
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: Thur Aug 15 23:40:14 EDT 2002
//------------------------------------------------------------------------------
#ifndef ASSA_H
#define ASSA_H

// This is the minimum set of header files that includes
// all ASSA classes definition.

#include <assa/Acceptor.h>
#include <assa/Address.h>
#include <assa/AutoPtr.h>
#include <assa/CommonUtils.h>
#include <assa/Connector.h>
#include <assa/CharInBuffer.h>
#include <assa/ConUDPSocket.h>
#include <assa/FileLogger.h>
#include <assa/GenServer.h>
#include <assa/INETAddress.h>
#include <assa/IPv4Socket.h>
#include <assa/Logger.h>
#include <assa/MemDump.h>
#include <assa/Pipe.h>
#include <assa/PriorityQueue_STLPQ.h>
#include <assa/Repository.h>
#include <assa/Semaphore.h>
#include <assa/SigAction.h>
#include <assa/SigHandler.h>
#include <assa/SigHandlers.h>
#include <assa/SigHandlersList.h>
#include <assa/Socketbuf.h>
#include <assa/Socket.h>
#include <assa/StdOutLogger.h>
#include <assa/Streambuf.h>
#include <assa/UDPSocket.h>
#include <assa/UnConUDPSocket.h>
#include <assa/UNIXAddress.h>
#include <assa/xdrIOBuffer.h>

#endif /* ASSA_H */
