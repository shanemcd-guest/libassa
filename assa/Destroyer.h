// -*- c++ -*-
//------------------------------------------------------------------------------
//                            Destroyer.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef _Destroyer_h
#define _Destroyer_h

namespace ASSA {

/** @file Destroyer.h 

	Destroyer is the helper class for class Singleton.
	@see Singleton
*/

template <class T>
class Destroyer 
{
public:
	/** Constructor.
	    @param d_ pointer to the object to guard.
	*/
	Destroyer(T* d_=0) : m_otg (d_) { /* empty */ }
	
	/* Destructor - deletes object T it owns.
	 */
	~Destroyer() {
		if ( m_otg ) {
			delete m_otg;
		}
	}

	/** Transfer ownership of object T to Destroyer class.
	    @param d_ - object T to guard
	*/
	void setGuard(T* d_) {
		m_otg = d_;
	}

private:
	Destroyer(const Destroyer<T>&);
	Destroyer<T>& operator=(const Destroyer<T>&);

private:
	/// Object T to guard
	T* m_otg;			
};

} // end namespace ASSA

#endif
