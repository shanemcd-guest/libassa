// -*- c++ -*-
//------------------------------------------------------------------------------
//                            Singleton.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002,2005  Vladislav Grinchenko
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 02/22/99
//------------------------------------------------------------------------------
#ifndef _Singleton_h
#define _Singleton_h

#include "Destroyer.h"

namespace ASSA {

/**  @file Singleton.h 

Singleton template class allows to turn any new or existing class T 
into Singleton Pattern. It is accomplished by deriving class T from
class Singleton.

It is assumed that Singleton objects are long-lived. Often they exist
for the life of the program. You delete them not so much to reclaim space
but to shut down in the orderly manner (such as return whatever 
resources derived class holds in its ownership back to the system). 
C++ deletes static objects automatically. Although, it doesn't guarantee
the calling order. In other words, destructors of Singleton class are not
order-dependent. 

To force destruction order, Singleton class transfers ownership of 
object T to Destroyer class. When the program exits, the Destroyer will
be destroyed, and the object T along with it. Singleton destructor is
now implicit.
*/

template <class T>
class Singleton
{
public:
	/// Return an instance of templated class T

	static T* get_instance () {
		if (m_instance == 0) {
			m_instance = new T;
			m_destroyer.setGuard (m_instance);
		}
		return m_instance;
	}

protected:
	/// Protected Constructor
	Singleton() {}
	
	friend class Destroyer<T>;
	
	/// Virtual Destructor
	virtual ~Singleton () {}
	
private:
	/// Pointer to the object T instance
	static T* m_instance;
	
	/// Destroyer that owns object T
	static Destroyer<T> m_destroyer;
};

} // end namespace ASSA


/** @def ASSA_DECL_SINGLETON(K)

    ASSA_DECL_SINGLETON macro inserts static member declarations
    mandated by the Singleton class.

	'assa-genesis' utility generates appropriate file templated by default.
*/
#define ASSA_DECL_SINGLETON(K)	\
template <> K* ASSA::Singleton<K>::m_instance = NULL;  \
template <class T> ASSA::Destroyer<T> ASSA::Singleton<T>::m_destroyer; \
template ASSA::Destroyer<K> ASSA::Singleton<K>::m_destroyer;

#endif
