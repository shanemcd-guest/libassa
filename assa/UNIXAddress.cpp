// -*- c++ -*-
//------------------------------------------------------------------------------
//                           UNIXAddress.C
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
//  Created: 03/22/99
//------------------------------------------------------------------------------

#include "assa/UNIXAddress.h"

#if !defined (WIN32)

using namespace ASSA;

UNIXAddress::
UNIXAddress (const char* socket_name_)
{
	trace("UNIXAddress::UNIXAddress(char* name_)");
	
	size_t len;
	m_address.sun_family = AF_UNIX;
	
	if ( (len = strlen(socket_name_)) > sizeof(m_address.sun_path) ) {
		EL((ASSAERR,"Socket path name is too long (%d bytes)\n", len));
		setstate (Address::badbit);
	}
	strcpy (m_address.sun_path, socket_name_);
}

UNIXAddress::
UNIXAddress (SA* saddr_)
{
	trace("UNIXAddress::UNIXAddress(SA_UN*)");

	SA_UN* sa_un = (SA_UN*) saddr_;
	m_address.sun_family = AF_UNIX;
	
	size_t len = strlen(sa_un->sun_path);

	if ( len > sizeof (m_address.sun_path) - 1 ) {
		EL((ASSAERR,"Socket path name is too long (%d bytes)\n", len));
		setstate (Address::badbit);
	}
	strcpy(m_address.sun_path, sa_un->sun_path);
}

#endif /* !def WIN32 */
