// -*- c++ -*-
//------------------------------------------------------------------------------
//                               FdSet.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef FDSET_H
#define FDSET_H

/** @file FdSet.h */

#include <string.h>
#include <sys/time.h>			/* select(3) */

#if defined(Linux)				/* select(3) */
#    include <sys/types.h>
#    include <unistd.h>
#endif

#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
#include <list>

#include "assa/Logger.h"

namespace ASSA {

/** Class FdSet.

    Wrapper around struct fd_set. This class hides the differences
	between UNIX/POSIX and WIN32 implementations  of fd_set data structure.

	The main difference is that while fd_set on POSIX system is represented
	as bit flags, the same structure on WIN32 system is opaque and not limited
	by FD_SETSIZE. 

	In fact, it is represented as an array of SOCKETs (read u_int[FD_SETSIZE])
    along with the number of FDs in the set. This allows a WIN32 socket 
	descriptor value to fall anywhere in the range from 0 to max(u_int -1).

	handler_t type hides the type difference.
*/

class FdSet : public fd_set
{
public:
	/** Constructor. 
	 */
	FdSet ();

	/** Set flag (ON) for the argument fd.
	    @param  fd_ Bit to set.
	    @return false if argument is out of bounds, true otherwise.
	 */
	bool setFd (handler_t fd_);

	/** Clear flag (OFF) for the argument fd.
	    @param fd_ Bit to clear
	    @return false if argument is out of bounds; true otherwise.
	 */
	bool clear (handler_t fd_);

	/** Test whether fd's flag is on.
	    @param fd_ Bit to test
	    @return true if fd_ bit is set; false otherwise
	 */
	bool isSet (handler_t fd_);

	/** Sync internals after used by select(3C)
	 */
	void sync ();

	/** Reset every bit in the set (OFF).
	 */
	void reset ();


	/** Find out the highest file descriptor in the set
		@return highest value of file descriptor.
	 */
	int maxInSet ();

	/** Determine how many bits are set (ON) in the set.
	    @return Number of bits set
	 */
	int numSet ();

	/** Determine highest handler in the set.
		@return highest value in the set
	*/

	/** Write to debug log all bits set.
	 */
	void dump  ();

	/** Return object state dump as an ASCII string.
	 */
	std::string dump_c_str ();

private:

#if !defined (WIN32)
	typedef std::list<u_int>::iterator ActiveFDs_Iter;

    std::list<u_int> m_actfds;	/// A shortcut - list of active FDs
#endif
};

//------------------------------------------------------------------------------
// Member Functions
//------------------------------------------------------------------------------
inline      FdSet::FdSet  () { reset (); }
inline void FdSet::dump   () { DL ((REACT, "%s\n", dump_c_str ().c_str ())); }

inline bool FdSet::isSet  (handler_t fd_) { return FD_ISSET (fd_, this); }

inline int
FdSet::
numSet () 
{ 
#if defined (WIN32)
	return this->fd_count; 
#else  /* UNIX */
	return m_actfds.size ();
#endif
}


} // end namespace ASSA

#endif /* FDSET_H */  
