// -*- c++ -*-
//------------------------------------------------------------------------------
// $Id: FileLogger.h,v 1.4 2005/10/08 02:42:00 vlg Exp $
//------------------------------------------------------------------------------
//                            FileLogger.h
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#ifndef FILE_LOGGER_H
#define FILE_LOGGER_H

#include <fstream>
#include <string>
#include <iostream>

#include "assa/Logger_Impl.h"

namespace ASSA {

/** @file FileLogger.h
	
    Implemention of a Logger as a disk-based file.
*/

class FileLogger : public Logger_Impl
{
public:
	FileLogger ();

	virtual int log_open (const char* logfname_, 
						  u_long groups_,
						  u_long maxsize_ = 10485760); // 10 Mb = 1024x1024x10

	virtual int log_close  (void);
	virtual void log_resync (void);

	virtual int log_msg (Group g_, 
						 size_t indent_level_,
						 const string& func_name_,
						 size_t expected_sz_,
						 const char* fmt_, va_list);
	virtual int log_func (Group g_, 
						  size_t indent_level_,
						  const string& func_name_,
						  marker_t type_);

	/** Log message as it is (raw) without indentation or 
		timestamping, but still perform byte counting and 
		the logic associated with it.
	*/
	int log_raw_msg (const string& msg_);
	void dump (void);

private:
	enum state_t { opened, closed };

	FileLogger (const FileLogger&);            // copy constructor
	FileLogger& operator= (const FileLogger&); // copy assignment
	
	int handle_rollover ();

private:
	std::ofstream  m_sink;
	u_long    m_maxsize;
	state_t   m_state;
	u_long    m_bytecount;
};

inline FileLogger::
FileLogger () 
    : m_maxsize   (1048576), 
      m_state     (closed),
      m_bytecount (0)
{
	/*--- empty ---*/
}

inline void 
FileLogger::
log_resync (void)
{
	m_sink << std::flush;
}

} // end namespace ASSA

#endif /* FILE_LOGGER_H */
