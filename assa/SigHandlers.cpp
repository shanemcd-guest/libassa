// -*- c++ -*-
//------------------------------------------------------------------------------
//                            SigHandlers.cpp
//------------------------------------------------------------------------------
//  Copyright (C) 1997-2002  Vladislav Grinchenko 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Library General Public
//  License as published by the Free Software Foundation; either
//  version 2 of the License, or (at your option) any later version.
//------------------------------------------------------------------------------
#include "assa/SigHandlers.h"

using namespace ASSA;

#if !defined(WIN32)

//---------------------------------------------------------------------------
// static declarations
//---------------------------------------------------------------------------

SigHandlersList* SigHandlersList::m_instance[NSIG];

void
SigHandlers::
sighandlers_dispatcher (int signum_)
{
    trace_with_mask("SigHandlers::sighandlers_dispatch", SIGHAND);

    DL((SIGHAND,"==> Recevied signal # %d\n", signum_));
    dispatch (signum_);
}

int 
SigHandlers::
install (int            signum_,
		 EventHandler*  new_hand_,
		 SigAction*     new_disp_, 
		 EventHandler** old_hand_,	
		 SigAction*     old_disp_)  
{
    /*
      Retrieve current signal disposition. If 3rd party handler has 
      already been istalled, make CFUNC_Handler out of it, and put it in 
      the list with id=0. 
	  
      Add new_hand_ to the list. Has global sighandlers_dispatcher not 
      been installed yet, install it too.
    */

    trace_with_mask("SigHandlers::install()", SIGHAND);

    if (!in_range(signum_) == -1) {
		EL((ASSAERR,"in_range (%s) failed\n",signum_));
		return -1;
    }

    CFUNC_Handler* cfhp = NULL;
    SigHandlersList* handlist = NULL;

    handlist = SigHandlersList::instance(signum_);

    /*--- Retrieve current signal disposition ---*/

    SigAction cd;
    cd.retrieve_action(signum_);

    /*
      Check whether 3rd party software has already installed 
      signal handler. 
    */
    if ( cd.handler() != (C_SIG_HANDLER) sighandlers_dispatcher &&
		 cd.handler() != SIG_IGN && 
		 cd.handler() != SIG_DFL ) 
    {
    /*
	  Looks like some other code got ahead of me and installed C-function 
	  signal handler. Make a note of it.  
	  
	  Create EventHandler to hold 3rd party handler. This handler will be 
	  deleted only by SigHandlers::remove (NULL), when application demanded 
	  to remove all of the handlers.
	*/
		DL((SIGHAND,"Detected 3rd party \"C\" handler!\n"));
		
		cfhp = new CFUNC_Handler (cd.handler ());
		handlist->cfunc_handler (cfhp);
		
		/*
		  Insert 3rd party handler in list of handlers 
		  for this signal.
		*/
		DL((SIGHAND,"Adding 3rd party \"C\" handler\n"));
		
		if ( handlist->insert (cfhp) == false ) {
			EL((ASSAERR, "Failed to insert "\
				"c_func_handler for signum %d\n", signum_));
			delete (cfhp);
			handlist->cfunc_handler (0);
			return -1;
		}
		DL((SIGHAND,"Set size: %d\n", handlist->size () ));
    }
    /*--- Add new_hand_ to the list of handlers for signum_. ---*/
	
    DL((SIGHAND,"Adding EventHandler to the list\n"));

    if (handlist->insert (new_hand_) == false) {
		/*---
		  I failed to install new handler and might have already
		  added 3rd party CFUNC_Handler to the list without altering
		  disposition - if that's true, clean up the list.
		  ---*/
		EL((ASSAERR,"failed to add new_hand_ to handlers list\n"));

		if (handlist->seen_cfunc_handler () &&
			handlist->size() == 1) 
		{
			handlist->erase ();
			handlist->cfunc_handler (0);
		}
		return -1;
    }
    DL((SIGHAND,"Set size: %d\n", handlist->size () ));

    /*--- Has sighandlers_dispatcher been already installed? ---*/

    if (cd.handler() == (C_SIG_HANDLER) sighandlers_dispatcher) {
		return 0;
    }
    DL((SIGHAND,"Installing 'sighandlers_dispatcher'\n"));

    /*
      Installing new disposition; if user forgot to give me one
      then default will be used. 
    */
    SigAction sa ((C_SIG_HANDLER) SIG_DFL);

    if (new_disp_ == 0) {
		new_disp_ = &sa;
    }
	
    new_disp_->handler ((C_SIG_HANDLER) sighandlers_dispatcher);
	    
    if (new_disp_->register_action (signum_, old_disp_) == -1) {
		/*---
		  I failed to install sighandlers_dispatcher. Up to this
		  point, if application had conventional C handler installed,
		  it still remains active. Handlers list built so far is
		  meaningless - get rid of it. ---*/
		
		EL((ASSAERR,"register_action() error\n"));
		
		if (handlist->seen_cfunc_handler ()) {
			handlist->erase ();
			handlist->cfunc_handler (0);
			delete cfhp;
		}
		handlist->erase (new_hand_);
		return -1;
    }
    return 0;
}

int
SigHandlers::
remove (int signum_, EventHandler* eh_,
		SigAction* new_disp_, SigAction* old_disp_)

{
    trace_with_mask("SigHandlers::remove()", SIGHAND);

    if (in_range (signum_)) {
		EL((ASSAERR, "singum_ %d is out of range\n", signum_));
		return -1;
    }

    CFUNC_Handler* Cfhp = NULL;	// pointer to C-function event handler
    EventHandler* ehp = NULL;	// pointer to current event handler

    SigHandlersList& handlist = *(SigHandlersList::instance(signum_));
	
    if (eh_ == NULL) {
		DL((SIGHAND,"Erasing the entire set\n"));
		/*--- Erase an entire list. ---*/
		handlist.erase ();
		DL((SIGHAND,"Set size: %d\n", handlist.size ()));
    }
    else {
        /*
		  Note: I cannot do erasure in the same loop for following  reason:
		  
		  According to Stroustrup (Section 17.4.1.7):
		  "After erase(), the iterator cannot be used again because
		  the element to which it pointed is no longer there."
		  
		  According to STL Tutorial and Ref. Guide:
		  "The erase function invalidates all iterators to all
		  positions past the point of erasure."
		  
		  That's why here we first take care of id recycling and heap memory 
		  deallocation, and only then clean() the map all at once.
		*/
		SigHandlersList::iterator it;
		
		if ((it = handlist.find (eh_)) != handlist.end ()) {
			DL((SIGHAND,"Removing EventHandler\n"));
			ehp = (*it);
			handlist.erase (it);
		}
		DL((SIGHAND,"Set size: %d\n", handlist.size () ));
    }
    /*--- If set is not empty, we're done ---*/
    if (handlist.size ()) return 0;

    /* If map was emptied out, install new disposition
       with the 3rd party "C" function handler, if we had it.
    */
    SigAction null_sa;
    if (new_disp_ == 0) new_disp_ = &null_sa;

    DL((SIGHAND,"Handlers List is empty\n"));
	
    if (handlist.seen_cfunc_handler ()) {
		/*--- Put 3rd party handler into disposition  ---*/
		DL((SIGHAND,"Reinstalling \"C\" handler\n"));
		Cfhp = handlist.cfunc_handler (0);
		new_disp_->handler (Cfhp->handler ());
		delete Cfhp;
    }
    /*--- Install new disposition ---*/
    return new_disp_->register_action (signum_, old_disp_);
}

void
SigHandlers::
dispatch (int signum_)
{
    trace_with_mask("SigHandlers::dispatch", SIGHAND);

    /*---
      For every element in the set that holds all EventHandlers for
      given signum, call its respective handle_signal() member function.
      ---*/

    /*--- save errno ---*/
    int errno_saved = errno;

    SigHandlersList& handlist = *(SigHandlersList::instance(signum_));
    SigHandlersList::iterator it;
    EventHandler* ehp;
    
    for (it=handlist.begin(); it != handlist.end(); it++) {
		ehp = *it;
		if (ehp->handle_signal (signum_) == -1) {
			/*---
			  this event handler reported error when handling
			  signum - remove it from the set
			  ---*/
			handlist.erase (it);
		}
    }
    /*--- restore errno ---*/
    errno = errno_saved;
}

#endif // !defined(WIN32)

