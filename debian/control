Source: libassa
Priority: optional
Section: libs
Maintainer: Shane McDonald <mcdonald.shane@gmail.com>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13), doxygen, libtirpc-dev, pkgconf
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://libassa.sourceforge.net/
Vcs-Git: https://salsa.debian.org/shanemcd-guest/libassa.git
Vcs-Browser: https://salsa.debian.org/shanemcd-guest/libassa

Package: libassa-3.5-5-dev
Section: libdevel
Architecture: any
Depends: libtirpc-dev, ${shlibs:Depends}, libassa-3.5-5t64 (= ${binary:Version}),
  ${misc:Depends}
Conflicts: libassa0-dev, libassa3.4-0-dev, libassa3.5-5-dev
Replaces: libassa0-dev, libassa3.4-0-dev, libassa3.5-5-dev
Description: object-oriented C++ networking library (development files)
 libASSA is an object-oriented C++ networking library based on Adaptive
 Communication Patterns. It features a simplistic implementation of the set
 of communication patterns such as Service Configurator, Reactor, Acceptor,
 Connector, and others described in various papers published by
 Dr. D. C. Schmidt.
 .
 libASSA happily co-exists with other frameworks such as GUI toolkits and
 various CORBA implementations
 .
 This package contains the header files and static libraries which are
 needed for developing applications and a program that generates
 skeleton files for RAD development with ASSA library.

Package: libassa-3.5-5t64
Provides: ${t64:Provides}
X-Time64-Compat: libassa-3.5-5v5
Breaks: libassa-3.5-5v5 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libassa0, libassa3.5-5
Replaces: libassa-3.5-5v5, libassa0, libassa3.5-5
Description: object-oriented C++ networking library
 libASSA is an object-oriented C++ networking library based on Adaptive
 Communication Patterns. It features a simplistic implementation of the set
 of communication patterns such as Service Configurator, Reactor, Acceptor,
 Connector, and others described in various papers published by
 Dr. D. C. Schmidt.
 .
 libASSA happily co-exists with other frameworks such as GUI toolkits and
 various CORBA implementations
 .
 This package contains the shared libraries.
